/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[][]}
 */
const fourSum = (nums, target) => {
  if (nums.length < 4) return [];
  nums.sort((a, b) => a - b);
  let results = [];
  let outerStartPointer = 0;
  const lastIndex = nums.length - 1;
  let outerEndPointer = lastIndex;
  const averageValue = target / 4;
  outerLoop: while (outerStartPointer < lastIndex - 2) {
    let innerStartPointer = outerStartPointer + 1;
    let innerEndPointer = outerEndPointer - 1;
    if (
      nums[outerStartPointer] <= averageValue ||
      averageValue >= nums[outerEndPointer]
    ) {
      innerLoop: while (innerStartPointer < innerEndPointer) {
        const sum =
          nums[outerStartPointer] +
          nums[innerStartPointer] +
          nums[innerEndPointer] +
          nums[outerEndPointer];
        if (sum === target) {
          const candidateResult = [
            nums[outerStartPointer],
            nums[innerStartPointer],
            nums[innerEndPointer],
            nums[outerEndPointer],
          ];
          if (!checkResultExist(results, candidateResult))
            results.push(candidateResult);
          innerStartPointer++;
        } else if (sum < target) innerStartPointer++;
        else if (sum > target) innerEndPointer--;
      } // end of inner loop
    }

    if (outerEndPointer > outerStartPointer + 3) outerEndPointer--;
    else outerStartPointer++, (outerEndPointer = lastIndex);
  }
  return results;
};

const checkResultExist = (results, candidateResult) => {
  let resultExist = false;
  resultsIteration: for (let i = 0; i < results.length; i++) {
    if (results[i].every((value, index) => candidateResult[index] === value)) {
      resultExist = true;
      break;
    }
  }
  return resultExist;
};

const case1 = [[1, 0, -1, 0, -2, 2], 0];
const case2 = [[2, 2, 2, 2, 2], 8];
const case3 = [[-3, -1, 0, 2, 4, 5], 0];
const result = fourSum(...case2);
console.log(result);

/*
  if nums length < 4 return []
  sort nums array
  set results array | init []
  set 2 outer pointers | outerStartPointer init 0 | outerEndPointer init last index
  start while loop with condition outerStartPointer < last index - 2 | set label outer loop
      set 2 inner pointers | innerStartPointer init outerStartPointer + 1 | innerEndPointer init outerEndPointer - 1
      start while loop with condition innerStartPointer < innerEndPointer | set label inner loop
          set variable resultExist to false
          set candidateResult array includes all 4 nums of pointer values
          check the results arrays if any of them identical to candidateResult
          if sum of all 4 nums pointers = target
              set variable resultExist to false
              set candidateResult array includes all 4 nums of pointer values
              check the results arrays if any of them identical to candidateResult
              if resultExist is false push array has all nums of pointers values | increase innerStartPointer
          else if sum of all 4 nums pointers < target increase innerStartPointer
          else decrease innerEndPointer
      if outerEndPointer > outerStartPointer + 3 | decrease outerEndPointer
      else increase outerStartPointer & set outerEndPointer to last index
  return results
*/
