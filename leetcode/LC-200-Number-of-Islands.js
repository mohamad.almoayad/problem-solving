var numIslands = function (grid) {
  let islansCount = 0;
  let globalLandMap = {};
  const columnLength = grid.length;
  const rowLength = grid[0].length;
  let rowIndex = 0;
  let columnIndex = 0;

  while (rowIndex < rowLength && columnIndex < columnLength) {
    globalLandMap[rowIndex + "-" + columnIndex] = false;
    // if(rowIndex < rowLength) 
  }
};

const case1 = [
  ["1", "0", "0", "0"],
  ["0", "1", "0", "0"],
  ["0", "0", "1", "0"],
  ["0", "0", "0", "1"],
];

const result = numIslands(case1);
console.log(result);
