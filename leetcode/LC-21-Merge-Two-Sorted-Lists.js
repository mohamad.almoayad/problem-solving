/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
const mergeTwoLists = (list1, list2) => {
    if (list1 && !list2) return list1;
    else if (!list1 && list2) return list2;
    else if (!list1 && !list2) return null;

    let sortedList = {};
    let lastNode = sortedList;
    let listOneNode = list1;
    let listTwoNode = list2;

    while (listOneNode || listTwoNode) {

        if (listOneNode && listTwoNode) {
            if (listOneNode['val'] <= listTwoNode['val']) {
                lastNode['val'] = listOneNode['val'];
                listOneNode = listOneNode['next'];
            } else {
                lastNode['val'] = listTwoNode['val'];
                listTwoNode = listTwoNode['next']
            }
        } else if (listOneNode) {
            lastNode['val'] = listOneNode['val'];
            listOneNode = listOneNode['next'];
        } else if (listTwoNode) {
            lastNode['val'] = listTwoNode['val'];
            listTwoNode = listTwoNode['next'];
        }
        if (!listOneNode && !listTwoNode) {
            lastNode['next'] = null;
        } else {
            lastNode['next'] = {};
            lastNode = lastNode['next'];
        }
    }
    return sortedList;
};

const case1 = [{
    val: 1,
    next: {
        val: 2,
        next: {
            val: 4,
            next: null
        }
    }
}, {
    val: 1,
    next: {
        val: 3,
        next: {
            val: 4,
            next: null
        }
    }
}];
const result = mergeTwoLists(...case1);
console.log(result);