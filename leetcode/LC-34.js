const searchRange = (nums = [], target) => {
    let leftPosition = 0;
    let rightPosition = nums.length - 1;
    let numsLength = nums.length;
    let numsMidIndex = Math.floor(numsLength / 2);

    // if (target < nums[numsMidIndex]) {
    //     numsMidIndex = Math.floor(numsMidIndex / 2);
    // } else if (target > nums[numsMidIndex]) {
    //     numsMidIndex = Math.floor((numsLength + numsMidIndex) / 2);
    // }

    // leftPosition = numsMidIndex;
    // rightPosition = numsMidIndex;
    // while (true) {
    //     let shouldContinue = 0;
    //     if (nums[leftPosition - 1] === target) leftPosition--, shouldContinue++;
    //     if (nums[rightPosition + 1] === target) rightPosition++, shouldContinue++;
    //     if (shouldContinue === 0) break;
    // }
    let firstIndex = null;
    let lastIndex = null;
    for (let i = 0; i < numsLength; i++) {
        if (nums[leftPosition] !== target) leftPosition++;
        else if (!firstIndex && nums[leftPosition] === target) firstIndex = leftPosition;
        if (nums[rightPosition] !== target) rightPosition--;
        else if (!lastIndex && nums[rightPosition] === target) lastIndex = rightPosition;
        if (firstIndex && lastIndex) break;
        if (rightPosition < leftPosition) break;
    }
    console.log(leftPosition, rightPosition, firstIndex === null, !lastIndex);
    if (firstIndex === null && lastIndex === null) return [-1, -1];
    return [firstIndex, lastIndex];

};

searchRange([1], 1);
// searchRange([5, 7, 7, 8, 8, 10], 8);