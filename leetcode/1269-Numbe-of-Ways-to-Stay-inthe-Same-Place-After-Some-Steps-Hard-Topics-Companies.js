/**
 * @param {number} steps
 * @param {number} arrLen
 * @return {number}
 */
var numWays = function (steps, arrLen) {
  let waysNumber = 0;
  let edge = arrLen - 1;

  const recursive = (leftSteps, currentIndex = 0) => {
    if (leftSteps === 0) {
      waysNumber++;
      return;
    }
    if (currentIndex) recursive(leftSteps - 1, currentIndex - 1);
    if (currentIndex < edge && leftSteps > currentIndex + 1)
      recursive(leftSteps - 1, currentIndex + 1);
    if (leftSteps > currentIndex) recursive(leftSteps - 1, currentIndex);
  };

  recursive(steps, 0);
  return waysNumber;
};

// console.log(numWays(2, 6));
// console.log(numWays(4, 6));
// console.log(numWays(8, 6));
// console.log(numWays(16, 6));
// Constraints of movements
// 1- no left if in index 0
// 2- no right if in the end of array
// 3- no right Or stay if left of steps less than current index
const arr = [9, 5, 6, 3];
const str = "abcdef";
console.log([...arr.entries()]);
console.log(str.split(""));

