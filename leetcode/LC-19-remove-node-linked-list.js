const removeNthFromEnd = (head, n) => {
  let header = head;
  let listLength = 1;
  while (header.next) {
    header = header.next;
    listLength++;
  }
  if (listLength === 1) return null;
  let targetNodexIndex = listLength - n + 1;
  if (targetNodexIndex === 1) return head.next;
  let prevTargetNodeIndex = listLength - n;
  let nextTargetNode;
  header = head;
  for (let i = 1; i <= listLength; i++) {
    if (i === targetNodexIndex) {
      nextTargetNode = header.next;
      break;
    }
    header = header.next;
  }
  header = head;
  for (let i = 1; i <= prevTargetNodeIndex; i++) {
    if (i === prevTargetNodeIndex) {
      header.next = nextTargetNode;
      break;
    }
    header = header.next;
  }
  header = head;
  while (header.next) {
    console.log(header);
    header = header.next;
  }
  return head;
};

removeNthFromEnd(
  {
    val: 1,
    next: {
      val: 2,
      next: {
        val: 3,
        next: {
          val: 4,
          next: {
            val: 5,
            next: null,
          },
        },
      },
    },
  },
  2
);
