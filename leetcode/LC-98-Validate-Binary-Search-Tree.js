const isValidBST = (root) => {
    // console.log("root",  root.val)
    if(!root.left && !root.right) return true;
    let result = true;
    function checkValidTreeRecursivly(node) {
        if(!result) return false;
        if(node.left && node.right){
            if(node.left.val >= node.val) result = false;
            if(node.right.val <= node.val)  result = false;
            if(result) checkValidTreeRecursivly(node.left);
            if(result) checkValidTreeRecursivly(node.right);
        }else if(!node.left && !node.right) {
            
        }else{
            result = false;
        }
        if(node.left) {
            // console.log("left", node.left.val, node.val, node.left.val >= node.val)

        }
        if(node.right) {
            // console.log("right", node.right.val, node.val, node.right.val <= node.val)
          
            
        }
        
    }
    
    checkValidTreeRecursivly(root);
    return result;
    
};