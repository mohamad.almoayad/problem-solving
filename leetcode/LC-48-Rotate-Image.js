const rotate = (matrix = []) => {
    /* 
    [0,0] => [0,2]
    [0,2] => [2,2]
    [2,2] => [2,0]
    [2,0] => [0,0]

    [1,0] => [0,1]
    [0,1] => [1,2]
    [1,2] => [2,1]
    [2,1] => [1,0]

    [1,1] => [1,1]

    Each element:
        second index goes to next position first index
        first index goes to next position first index


    */
    const maxIndex = matrix.length - 1;
    let startPoint = [0, 0];
    while (true) {
        for (let x = startPoint[0]; x < maxIndex - startPoint[0]; x++) {
            let currentPosition = [startPoint[0], x];
            let tempValue = matrix[currentPosition[0]][currentPosition[1]];
            for (let y = 0; y < 4; y++) {
                let nextY = maxIndex - currentPosition[0];
                let temp = matrix[currentPosition[1]][nextY];
                matrix[currentPosition[1]][nextY] = tempValue;
                tempValue = temp;
                currentPosition = [currentPosition[1], nextY];
            }
        }
        startPoint = [startPoint[0] + 1, startPoint[1] + 1];
        if (startPoint[0] + startPoint[1] >= maxIndex) break;
    }
    console.log(matrix)
};

const result = rotate([
    [5, 1, 9, 11],
    [2, 4, 8, 10],
    [13, 3, 6, 7],
    [15, 14, 12, 16]
]);