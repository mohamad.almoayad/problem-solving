/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
const strStr = (haystack, needle) => {
  const needleFirstChar = needle.charAt(0);
  for (let i = 0; i < haystack.length; i++) {
    if (haystack.charAt(i) === needleFirstChar) {
      const haystackSlice = haystack.slice(i, i + needle.length);
      if (haystackSlice === needle) return i;
    }
    if (haystack.length - (i + 1) < needle.length) break;
  }
  return -1;
};

const case1 = ["hello", "ll"];
const case2 = ["aaaaa", "bba"];
const result = strStr(...case1);
console.log(result);

/*
set the first char of needle to variable needleFirstChar
start iterate on haystack characters
each lap compare check equality the haystack char on the i index with the needleFirstChar
if equal take a slice of the haystack equal the length of needle from the current index 
compare the slice with the needle
if true return i
check if the next slice length will be smaller than needle length break
return -1 at the end
*/
