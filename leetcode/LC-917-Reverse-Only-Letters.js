/**
 * @param {string} s
 * @return {string}
 */
var reverseOnlyLetters = function (s) {
  let leftPointer = 0;
  let rightPointer = s.length - 1;
  let leftPart = "";
  let rightPart = "";
  let leftChar;
  let rightChar;
  let conditionleft;
  let conditionRight;
  while (leftPointer < rightPointer) {
    leftChar = s.charAt(leftPointer).toLowerCase().charCodeAt(0);
    rightChar = s.charAt(rightPointer).toLowerCase().charCodeAt(0);
    conditionleft = leftChar < 97 || leftChar > 122;
    conditionRight = rightChar < 97 || rightChar > 122;
    if (!conditionleft && !conditionRight) {
      leftPart = leftPart + s.charAt(rightPointer);
      rightPart = s.charAt(leftPointer) + rightPart;
      leftPointer++;
      rightPointer--;
    } else {
      if (conditionleft) {
        leftPart = leftPart + s.charAt(leftPointer);
        leftPointer++;
      }
      if (conditionRight) {
        rightPart = s.charAt(rightPointer) + rightPart;
        rightPointer--;
      }
    }
  }
  if (leftPointer === rightPointer) leftPart = leftPart + s.charAt(leftPointer);
  return leftPart + rightPart;
};
