/* const combinationSum = (candidates = [], target = 0) => {
  let candidatesLength = candidates.length;
  let leftIndex = 0;
  let rightIndex = candidatesLength - 1;
  let sumCombinations = [];
  while (leftIndex !== rightIndex) {
    let rightCandidate = candidates[rightIndex];
    let leftCandidate = candidates[leftIndex];
    if (rightCandidate > target) {
      candidates.splice(rightIndex, 1);
      rightIndex--;
      continue;
    }
    if (rightCandidate === target) {
      sumCombinations.push(rightCandidate);
      candidates.splice(rightIndex, 1);
      continue;
    }
    if (rightCandidate < target) {
      let targetRest = target - rightCandidate;
      let container = [];
      let potentialCandidates = [];
      let tempSum = rightCandidate;
      if (target % rightCandidate === 0) {
        for (let i = 0; i < target / rightCandidate; i++) {
          container.push(rightCandidate);
          sumCombinations.push(container);
          container = [];
        }
      }
      for (let i = 0; i < rightIndex; i++) {
        if (rightCandidate + sumCombinations[i] > target) break;
        if (targetRest % sumCombinations[i] === 0) {
          for (let i = 0; i < targetRest / sumCombinations[i]; i++) {
            container.push(sumCombinations[i]);
          }
          sumCombinations.push([rightCandidate, ...container]);
          container = [];
        }
        if (tempSum + sumCombinations[i] <= target) {
          tempSum += sumCombinations[i];
          potentialCandidates.push(sumCombinations[i]);
          if (tempSum === target) {
            sumCombinations.push([rightCandidate, ...potentialCandidates]);
          }
        }
      }
    }
  }
};
console.log(8 >> 1);
// combinationSum([2, 3, 6, 7], 7); */

let candidates = [2, 3, 4, 5, 7];
let target = 7;
let sumCombinations = [];

function combineRecursive(index, total, container) {
  if (total >= target || index === candidates.length) return;

  for (let i = index; i < candidates.length; i++) {
    let tempContainer = [...container];
    let subTotal = total;
    subTotal += candidates[i];
    tempContainer.push(candidates[i]);
    if (subTotal === target) sumCombinations.push(tempContainer);
    else combineRecursive(i, subTotal, tempContainer);
  }
}

combineRecursive(0, 0, []);

// console.log(sumCombinations);
console.log("'" + 5 + "'");

// [1,2,3,4,7] TARGET 14
/* 
left = 1
right = target - 1
target = left + right
check if (left & right exist is candidates) => push to potentials

1,1 | 1,1,1  1,1,2   1,1,3   1,1,4   1,1,7
1,2
1,3
1,4
1,7
*/
