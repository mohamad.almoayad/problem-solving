const trapX = (height = []) => {
  if (height.length < 3) return 0;
  let segments = [];
  let result = 0;

  function prepareSegments(startIndex) {
    let nextEdgeHeight = null;
    for (let i = startIndex; i < height.length; i++) {
      if (segments.length === 0) {
        if (height[i] > 0) segments.push(i);
      } else {
        if (height[i] >= height[segments[segments.length - 1]]) {
          segments.push(i);
          nextEdgeHeight = null;
        } else if (!nextEdgeHeight || height[i] >= height[nextEdgeHeight])
          nextEdgeHeight = i;
      }
    }
    if (nextEdgeHeight) {
      segments.push(nextEdgeHeight);
    }
    if (nextEdgeHeight && nextEdgeHeight < height.length - 1)
      prepareSegments(nextEdgeHeight + 1);
  }
  prepareSegments(0);
  if (segments.length === 0) return 0;
  /* for (let x = 0; x < segments.length - 1; x++) {
    let startIndex = segments[x];
    let endIndex = segments[x + 1];
    let maxHeight = Math.min(height[startIndex], height[endIndex]);

    if (endIndex - startIndex > 1)
      for (let i = startIndex + 1; i < endIndex; i++) {
        result += maxHeight - height[i];
      }
  } */

  let startIndex = 0;
  let endIndex = 1;
  let maxHeight = Math.min(height[segments[startIndex]], height[segments[endIndex]]);
  for (let i = segments[0]; i < height.length; i++) {
    if (i > segments[startIndex] && i < segments[endIndex]) {
      result += maxHeight - height[i];
    }
    if (i === segments[endIndex] && segments.length - 1 > endIndex) {
      startIndex++;
      endIndex++;
      maxHeight = Math.min(height[segments[startIndex]], height[segments[endIndex]]);
    }
  }

  return result;
};

const trap = (height = []) => {
  if (height.length < 3) return 0;
  let filledWaterVolume = 0
  let firstEdge = -1;
  let lastEdge = -1;

  function calcInbetweenEdges(startIndex) {
    for (let i = startIndex; i < height.length; i++) {
      if (firstEdge === -1) {
        if (height[i] > 0) firstEdge = i;
      } else {
        if (height[i] >= height[firstEdge]) {
          lastEdge = i;
          break;
        } else if (lastEdge === -1 || height[i] >= height[lastEdge])
          lastEdge = i;
      }
    }

    maxHeight = Math.min(height[firstEdge], height[lastEdge]);
    for (let index = firstEdge + 1; index < lastEdge; index++) {
      filledWaterVolume += maxHeight - height[index];
    }

    if (lastEdge < height.length - 1) {
      firstEdge = lastEdge;
      lastEdge = -1;
      calcInbetweenEdges(firstEdge + 1);
    }
  }
  calcInbetweenEdges(0);
  return filledWaterVolume;
}

const case1 = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1];
const case2 = [0, 0, 0, 0];
const case3 = [5, 5, 5, 5];
const case4 = [1, 0, 0, 0, 1];
const case5 = [0, 0, 1, 0, 1];
const case6 = [0, 0, 1, 0, 0];
const case7 = [0, 1, 2, 3, 2, 1, 0];
const case8 = [3, 2, 1, 0, 1, 2, 3];
const result = trap(case1);
console.log(result);