/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function (pattern, s) {
  const strArray = s.split(" ");
  if (pattern.length != strArray.length) return false;
  const graph = {};
  for (let i = 0; i < strArray.length; i++) {
    const str = strArray[i] + "xyz";
    const char = pattern[i];
    if (!graph[str] && !graph[char]) {
      graph[str] = new Set();
      graph[str].add(char);
      graph[char] = new Set();
      graph[char].add(str);
    } else if (!graph[str] || !graph[char]) {
      return false;
    } else {
      graph[str].add(char);
      if (graph[str].size > 1) return false;
      graph[char].add(str);
      if (graph[char].size > 1) return false;
    }
  }

  return true;
};
