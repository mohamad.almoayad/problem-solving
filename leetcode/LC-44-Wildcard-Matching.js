const isMatch = (s = '', p = '') => {
    // let text = 'TESTnewTEXTTEST';
    // text = text.slice(14)
    // console.log(text);
    if (s === p) return true;
    let symbolCount = false;

    for (let i = 0; i < p.length; i++) {
        if (p.charAt(i) === '*') {
            symbolCount = true;
        } else if (p.charAt(i) === '?') {
            if (s.length > 0)
                s = s.slice(1);
            else return false;
        } else {
            if (symbolCount) {
                if (s.charAt(0) === p.charAt(i)) {
                    s = s.slice(1);
                } else {
                    const charIndex = s.indexOf(p.charAt(i));
                    if (charIndex === -1) return false;
                    s = s.slice(charIndex + 1);
                    symbolCount = false;
                }
            } else {
                console.log(s, p.charAt(i))
                if (s.charAt(0) !== p.charAt(i)) return false;
                s = s.slice(1);
            }
        }
    }
    console.log(s)
    if (s.length > 0 && !symbolCount) return false;

    // for (let i = 0; i < p.length; i++) {
    //     if (p.charAt(i) != '*' && p.charAt(i) != '?' && firstIndex === -1) {
    //         firstIndex = i;
    //         lastIndex = i;
    //     } else {
    //         if (p.charAt(i) != '*' && p.charAt(i) != '?') {
    //             lastIndex = i;
    //         } else {
    //             if (!s.includes(p.substring(firstIndex, lastIndex + 1))) return false;
    //             if (p.charAt(i) === '?') symbolCount++;
    //             const subString = p.substring(firstIndex, lastIndex + 1);
    //             s = s.replace(subString, '');
    //             newPattern = newPattern.replace(subString, '');
    //             firstIndex = -1;
    //             lastIndex = -1;
    //         }
    //     }

    // }
    // console.log('S: ', s);
    // newPattern = newPattern.slice(0, -1);
    // console.log('P: ', newPattern);
    // console.log('symbols: ', symbolCount);
    // if (s.length < symbolCount) return false;
    // if (s.length > symbolCount && !newPattern.includes('*')) return false;

    // let stringIndex = 0;
    // for (let i = 0; i < p.length; i++) {
    //     if (p.charAt(i) === s.charAt(stringIndex) || p.charAt(i) === '?') {
    //         stringIndex++;
    //     } else if (p.charAt(i) === '*') {
    //         let tempPattern = p.slice(0, i) + p.slice(i + 1, p.length);
    //         if (isMatch(s, tempPattern)) return true;
    //         let patternRest = p.length - i - 1;
    //         stringIndex = s.length - patternRest;
    //     } else return false;
    // }
    // if (stringIndex < s.length) return false;
    return true;
};

// const result = isMatch("HxABCDnEXFlD", "H*?ABCD?EXF*?D");
const result = isMatch("abcabczzzde", "*abc???de*");
// const result = isMatch("acdcb", "a*c?b");
console.log(result);