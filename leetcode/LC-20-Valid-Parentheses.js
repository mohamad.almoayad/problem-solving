const isValid = (s) => {
    if (s.length % 2 === 1) return false;
    const openedParenthesesMap = {
        '(': true,
        '{': true,
        '[': true
    };
    const parenthesesMap = {
        ')': '(',
        '}': '{',
        ']': '['
    };
    const openedParenthesesStack = [];

    for (let i = 0; i < s.length; i++) {
        const inFocusChar = s.charAt(i);
        const lastElementInStack = openedParenthesesStack[openedParenthesesStack.length - 1];
        if (inFocusChar in openedParenthesesMap) {
            openedParenthesesStack.push(inFocusChar);
        } else {
            if (openedParenthesesStack.length === 0) return false;
            if (parenthesesMap[inFocusChar] !== lastElementInStack) return false;
            openedParenthesesStack.pop();
        }
    }
    if (openedParenthesesStack.length > 0) return false;
    return true;

};
const case1 = "()";
const case2 = "()[]{}";
const case3 = "(]";
const result = isValid(case2);
console.log(result);