/**
 * @param {string} s
 * @return {string}
 */
var sortVowels = function (s) {
  const vowelsSet = new Set(["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]);
  let vowelsArr = [];
  let result = "";
  let hasVowels = false;
  for (let char of s) {
    if (vowelsSet.has(char)) {
      const vowel = [char, char.charCodeAt(0)];
      vowelsArr.push(vowel);
      if (!hasVowels) hasVowels = true;
    }
  }
  if (!hasVowels) return s;
  vowelsArr.sort((a, b) => b[1] - a[1]);
  for (let char of s) result += vowelsSet.has(char) ? vowelsArr.pop()[0] : char;

  return result;
};
