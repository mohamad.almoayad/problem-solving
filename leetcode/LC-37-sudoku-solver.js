const solveSudoku = (board) => {
  const numbersOptions = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
  /* REPEATED */
  function xyz(boardInstanse) {
    let whiteSpaces = [];
    let lastWhiteSpaceLegth = 0;
    while (true) {
      let columns = [];
      whiteSpaces = [];
      /* prepare array of columns & empty blocks in board */
      for (let x = 0; x < 9; x++) {
        let column = [];
        for (let y = 0; y < 9; y++) {
          column.push(boardInstanse[y][x]);
          if (boardInstanse[x][y] === ".")
            whiteSpaces.push({
              key: [x + "" + y],
              rowIndex: x,
              columnIndex: y,
              candidates: [],
            });
        }
        columns.push(column);
      }

      for (let i in whiteSpaces) {
        let numbersOptionsCopy = [...numbersOptions];
        let rowIndex = whiteSpaces[parseInt(i)]["rowIndex"];
        let columnIndex = whiteSpaces[parseInt(i)]["columnIndex"];
        let row = boardInstanse[rowIndex];
        let column = columns[columnIndex];
        let xMapIndex = Math.ceil((rowIndex + 1) / 3);
        let yMapIndex = Math.ceil((columnIndex + 1) / 3);
        let mapIndex = xMapIndex + "-" + yMapIndex;
        for (let n = 0; n < 9; n++) {
          let checkNumberExistanceIndex = numbersOptionsCopy.findIndex(
            (number) => number == row[n]
          );
          if (checkNumberExistanceIndex > -1)
            numbersOptionsCopy.splice(checkNumberExistanceIndex, 1);

          checkNumberExistanceIndex = numbersOptionsCopy.findIndex(
            (number) => number == column[n]
          );
          if (checkNumberExistanceIndex > -1)
            numbersOptionsCopy.splice(checkNumberExistanceIndex, 1);
        }
        maps[mapIndex].forEach((element) => {
          let xIndx = element["x"];
          let yIndx = element["y"];
          let checkNumberExistanceIndex = numbersOptionsCopy.findIndex(
            (number) => number == boardInstanse[xIndx][yIndx]
          );

          if (checkNumberExistanceIndex > -1)
            numbersOptionsCopy.splice(checkNumberExistanceIndex, 1);
        });
        /* Add potential candidates */
        whiteSpaces[parseInt(i)]["candidates"] = [...numbersOptionsCopy];
      }

      for (let i = 0; i < whiteSpaces.length; i++) {
        if (whiteSpaces[i]["candidates"].length === 1) {
          boardInstanse[whiteSpaces[i]["rowIndex"]][
            whiteSpaces[i]["columnIndex"]
          ] = String(whiteSpaces[i]["candidates"][0]);
          break;
        }
      }

      if (whiteSpaces.length === lastWhiteSpaceLegth) {
        return [boardInstanse, whiteSpaces];
      }
      lastWhiteSpaceLegth = whiteSpaces.length;
    }
  }

  let [firstBoardInstanse, whiteSpaces] = xyz(board);
  if (whiteSpaces.length === 0) {
    board = firstBoardInstanse;
    return board;
  }
  let firstCandidates = whiteSpaces[0]["candidates"];
  xyzRecursive(firstBoardInstanse, whiteSpaces, firstCandidates);

  function splitter(boardInstanse, whiteSpacesCopy, candidates) {
    if (candidates.length === 1) {
      return xyzRecursive(boardInstanse, whiteSpacesCopy, candidates);
    }

    let lefthand;
    let rightHand;
    if (candidates.length > 2) {
      let midCandidates = Math.floor(candidates.length / 2);
      lefthand = candidates.slice(0, midCandidates);
      rightHand = candidates.slice(midCandidates);

      splitter(boardInstanse, whiteSpacesCopy, lefthand);
      splitter(boardInstanse, whiteSpacesCopy, rightHand);
    } else if (candidates.length === 2) {
      splitter(boardInstanse, whiteSpacesCopy, [candidates[0]]);
      splitter(boardInstanse, whiteSpacesCopy, [candidates[1]]);
    }
  }

  function xyzRecursive(boardInstanse, whiteSpacesCopy, candidates) {
    // BASE CASE
    if (whiteSpacesCopy.length === 0) {
      board = boardInstanse;
      return;
    }
    // RECURSIVE CODE
    let recursiveBoard = [...boardInstanse];
    if (candidates.length > 1) {
      splitter(boardInstanse, [...whiteSpacesCopy], [...candidates]);
    } else if (candidates.length === 1) {
      recursiveBoard[whiteSpacesCopy[0]["rowIndex"]][
        whiteSpacesCopy[0]["columnIndex"]
      ] = String(candidates[0]);
      let tempArray = [];
      for (let i = 0; i < 9; i++) tempArray.push([...recursiveBoard[i]]);

      let [newBoardInstanse, newWhiteSpaces] = xyz(tempArray);

      return xyzRecursive(
        newBoardInstanse,
        [...newWhiteSpaces],
        newWhiteSpaces.length > 0 ? newWhiteSpaces[0]["candidates"] : []
      );
    }
  }
  return board;
};

const maps = {
  "1-1": [
    {
      x: 0,
      y: 0,
    },
    {
      x: 0,
      y: 1,
    },
    {
      x: 0,
      y: 2,
    },
    {
      x: 1,
      y: 0,
    },
    {
      x: 1,
      y: 1,
    },
    {
      x: 1,
      y: 2,
    },
    {
      x: 2,
      y: 0,
    },
    {
      x: 2,
      y: 1,
    },
    {
      x: 2,
      y: 2,
    },
  ],
  "1-2": [
    {
      x: 0,
      y: 3,
    },
    {
      x: 0,
      y: 4,
    },
    {
      x: 0,
      y: 5,
    },
    {
      x: 1,
      y: 3,
    },
    {
      x: 1,
      y: 4,
    },
    {
      x: 1,
      y: 5,
    },
    {
      x: 2,
      y: 3,
    },
    {
      x: 2,
      y: 4,
    },
    {
      x: 2,
      y: 5,
    },
  ],
  "1-3": [
    {
      x: 0,
      y: 6,
    },
    {
      x: 0,
      y: 7,
    },
    {
      x: 0,
      y: 8,
    },
    {
      x: 1,
      y: 6,
    },
    {
      x: 1,
      y: 7,
    },
    {
      x: 1,
      y: 8,
    },
    {
      x: 2,
      y: 6,
    },
    {
      x: 2,
      y: 7,
    },
    {
      x: 2,
      y: 8,
    },
  ],
  "2-1": [
    {
      x: 3,
      y: 0,
    },
    {
      x: 3,
      y: 1,
    },
    {
      x: 3,
      y: 2,
    },
    {
      x: 4,
      y: 0,
    },
    {
      x: 4,
      y: 1,
    },
    {
      x: 4,
      y: 2,
    },
    {
      x: 5,
      y: 0,
    },
    {
      x: 5,
      y: 1,
    },
    {
      x: 5,
      y: 2,
    },
  ],
  "2-2": [
    {
      x: 3,
      y: 3,
    },
    {
      x: 3,
      y: 4,
    },
    {
      x: 3,
      y: 5,
    },
    {
      x: 4,
      y: 3,
    },
    {
      x: 4,
      y: 4,
    },
    {
      x: 4,
      y: 5,
    },
    {
      x: 5,
      y: 3,
    },
    {
      x: 5,
      y: 4,
    },
    {
      x: 5,
      y: 5,
    },
  ],
  "2-3": [
    {
      x: 3,
      y: 6,
    },
    {
      x: 3,
      y: 7,
    },
    {
      x: 3,
      y: 8,
    },
    {
      x: 4,
      y: 6,
    },
    {
      x: 4,
      y: 7,
    },
    {
      x: 4,
      y: 8,
    },
    {
      x: 5,
      y: 6,
    },
    {
      x: 5,
      y: 7,
    },
    {
      x: 5,
      y: 8,
    },
  ],
  "3-1": [
    {
      x: 6,
      y: 0,
    },
    {
      x: 6,
      y: 1,
    },
    {
      x: 6,
      y: 2,
    },
    {
      x: 7,
      y: 0,
    },
    {
      x: 7,
      y: 1,
    },
    {
      x: 7,
      y: 2,
    },
    {
      x: 8,
      y: 0,
    },
    {
      x: 8,
      y: 1,
    },
    {
      x: 8,
      y: 2,
    },
  ],
  "3-2": [
    {
      x: 6,
      y: 3,
    },
    {
      x: 6,
      y: 4,
    },
    {
      x: 6,
      y: 5,
    },
    {
      x: 7,
      y: 3,
    },
    {
      x: 7,
      y: 4,
    },
    {
      x: 7,
      y: 5,
    },
    {
      x: 8,
      y: 3,
    },
    {
      x: 8,
      y: 4,
    },
    {
      x: 8,
      y: 5,
    },
  ],
  "3-3": [
    {
      x: 6,
      y: 6,
    },
    {
      x: 6,
      y: 7,
    },
    {
      x: 6,
      y: 8,
    },
    {
      x: 7,
      y: 6,
    },
    {
      x: 7,
      y: 7,
    },
    {
      x: 7,
      y: 8,
    },
    {
      x: 8,
      y: 6,
    },
    {
      x: 8,
      y: 7,
    },
    {
      x: 8,
      y: 8,
    },
  ],
};

const case1 = [
  ["5", "3", ".", ".", "7", ".", ".", ".", "."],
  ["6", ".", ".", "1", "9", "5", ".", ".", "."],
  [".", "9", "8", ".", ".", ".", ".", "6", "."],
  ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
  ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
  ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
  [".", "6", ".", ".", ".", ".", "2", "8", "."],
  [".", ".", ".", "4", "1", "9", ".", ".", "5"],
  [".", ".", ".", ".", "8", ".", ".", "7", "9"],
];
const case2 = [
  [".", ".", "9", "7", "4", "8", ".", ".", "."],
  ["7", ".", ".", ".", ".", ".", ".", ".", "."],
  [".", "2", ".", "1", ".", "9", ".", ".", "."],
  [".", ".", "7", ".", ".", ".", "2", "4", "."],
  [".", "6", "4", ".", "1", ".", "5", "9", "."],
  [".", "9", "8", ".", ".", ".", "3", ".", "."],
  [".", ".", ".", "8", ".", "3", ".", "2", "."],
  [".", ".", ".", ".", ".", ".", ".", ".", "6"],
  [".", ".", ".", "2", "7", "5", "9", ".", "."],
];
const case5 = [
  [".", ".", ".", ".", ".", "7", ".", ".", "9"],
  [".", "4", ".", ".", "8", "1", "2", ".", "."],
  [".", ".", ".", "9", ".", ".", ".", "1", "."],
  [".", ".", "5", "3", ".", ".", ".", "7", "2"],
  ["2", "9", "3", ".", ".", ".", ".", "5", "."],
  [".", ".", ".", ".", ".", "5", "3", ".", "."],
  ["8", ".", ".", ".", "2", "3", ".", ".", "."],
  ["7", ".", ".", ".", "5", ".", ".", "4", "."],
  ["5", "3", "1", ".", "7", ".", ".", ".", "."],
];

/* PERFECT SOLUTION */
/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudokuPerect = function (board) {
  //Auxiliary function to determine whether it is feasible to place a value at the current position
  function isOk(i, j, val) {
    //Determine whether the number already exists in the current line
    for (let k = 0; k < 9; k++) {
      if (board[i][k] == val) return false;
    }
    //Determine whether the number already exists in the current front row
    for (let k = 0; k < 9; k++) {
      if (board[k][j] == val) return false;
    }
    //Judge whether the number already exists in the current 3 * 3 lattice
    let l = Math.floor(j / 3) * 3,
      t = Math.floor(i / 3) * 3;
    for (let m = t; m < t + 3; m++) {
      for (let n = l; n < l + 3; n++) {
        if (board[m][n] == val) return false;
      }
    }
    return true;
  }
  function backTrace(i, j) {
    if (j === 9) {
      //Sudoku is finished, return true
      if (i === 8) return true;
      //Otherwise, go to the next line
      i++;
      j = 0;
    }
    //There is no number in the current position
    if (board[i][j] === ".") {
      //Select the number from 1-9 to fill in
      for (let val = 1; val <= 9; val++) {
        //If the current number meets the conditions, write it into Sudoku
        if (isOk(i, j, val)) {
          board[i][j] = val + "";
          //If there is a solution on this path, it will meet the condition and return directly
          if (backTrace(i, j + 1)) return true;
        }
        board[i][j] = ".";
      }
    } else {
      //There are numbers in the current position. Go directly to the next grid
      return backTrace(i, j + 1);
    }
    //If there are no numbers in the current box, and no numbers that meet the conditions can be filled in,
    //It means that this path is not working
    return false;
  }
  backTrace(0, 0);
  return board;
};

const result = solveSudokuPerect(case5);

console.log(result);
