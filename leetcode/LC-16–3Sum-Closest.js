/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
const threeSumClosest = (nums, target) => {
    let a = [1 , 3, 5, 7];
    let b = [1 , 3, 5, 7];
    console.log('check array equality', a.every((value, index) => b[index] === value));
  if (nums.length === 3) return nums[0] + nums[1] + nums[2];

  nums.sort((a, b) => a - b);

  const lastIndex = nums.length - 1;
  let startPointer = 1;
  let endPointer = lastIndex;
  let closestSum = nums[0] + nums[startPointer] + nums[endPointer];
  for (let index = 0; index < nums.length - 2; index++) {
    startPointer = index + 1;
    endPointer = lastIndex;
    while (startPointer < endPointer) {
      const candidateSum = nums[startPointer] + nums[index] + nums[endPointer];
      if (candidateSum === target) return candidateSum;
      else if (
        Math.abs(target - candidateSum) < Math.abs(target - closestSum)
      ) {
        closestSum = candidateSum;
      }
      if (candidateSum < target) startPointer++;
      else endPointer--;
    }
  }
  return closestSum;
//   while (startPointer < lastIndex - 1) {
//     const candidateSum = nums[startPointer] + nums[endPointer] + nums[index];
//     if (target === candidateSum) return target;
//     else if (Math.abs(target - candidateSum) < Math.abs(target - closestSum))
//       closestSum = candidateSum;
//     else if (
//       Math.abs(target - candidateSum) === Math.abs(target - closestSum) &&
//       candidateSum > closestSum
//     )
//       closestSum = candidateSum;
//     else if (Math.abs(target - candidateSum) > Math.abs(target - closestSum)) {
//       index === lastIndex;
//     }

//     if (endPointer + 1 === lastIndex) {
//       startPointer++;
//       endPointer = startPointer + 1;
//       index = endPointer + 1;
//     } else if (index === lastIndex) {
//       endPointer++;
//       index = endPointer + 1;
//     } else index++;
//   }
//   return closestSum;
};

const case1 = [[1, 2, 5, 8, 6], 10];
const case2 = [[-1, 0, 1, 1, 55], 3];
const case3 = [[0, 2, 1, -3], 1];
const case4 = [[-1, 2, 1, -4], 1];
const case5 = [[0, 0, 0], 1];
const case6 = [[-1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15], 19];
const case7 = [[-1, 1, 2, 3, 4, 5, 7, 9, 11, 15], 13];
const result = threeSumClosest(...case7);
console.log(result);

/*
if nums length === 3 return nums elements sum
sort nums ascending
set startPointer | init 1
set endPointer | init last index
set closestSum | init nums[0] + nums[1] + nums[2];
start for loop with index = 0 condition index < last index - 2 to let the spact to the other pointers
    set start pointer to index + 1
    set last pointer to last index
    get sum of nums[index] + nums[startPointer] + nums[endPointer]
    check if target === sum ? return target;
    else if the absolute value of( target - sum ) < absolute value of( target - closest ) ? set closestElement to sum
    if sum < target increase start pointer
    else sum > target decrease end pointer
return closestElement     
*/

/*
if nums length === 3 return nums elements sum
sort nums ascending
set startPointer | init nums[0]
set endPointer | init nums[1]
set index | init 3
set closestSum | init nums[0] + nums[1] + nums[2];
start while loop with condition primaryyElement < last index - 1
    get sum of nums[primary] + nums[secondary] + nums[index]
    check if target === sum ? return target;
    else if the absolute value of( target - sum ) < absolute value of( target - closest ) ? set closestElement to sum
    else if the absolute value of( target - sum ) === absolute value of( target - closest ) && sum > closest ? set closestElement to sum
    if endPointer + 1 === last index | increase startPointer | setsecondaryElement to startPointer + 1 | set index to endPointer + 1
    else if index === last index | increase endPointer | set index to endPointer + 1
    else increase index
return closestElement     
*/
