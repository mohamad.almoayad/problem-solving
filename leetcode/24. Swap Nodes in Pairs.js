/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var swapPairs = function (head) {
  let upHead = { val: -1, next: head };
  let currentNode = upHead;

  while (currentNode?.next?.next) {
    const firstNode = currentNode.next;
    currentNode.next = currentNode.next.next;
    firstNode.next = currentNode?.next?.next;
    currentNode.next.next = firstNode;
    currentNode = currentNode.next.next;
  }

  return upHead.next;
};
const head = {
  val: 1,
  next: {
    val: 2,
    next: {
      val: 3,
      next: {
        val: 4,
        next: null,
      },
    },
  },
};
console.log(swapPairs(head));
