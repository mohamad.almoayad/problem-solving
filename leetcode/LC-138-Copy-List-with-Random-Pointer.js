/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
const copyRandomList = (head) => {
  let newNodes = [];
  let currentOriginalNode = head;

  while (currentOriginalNode) {
    const newNode = {
      val: currentOriginalNode.val,
      next: null,
      random: null,
    };
    newNodes.push(newNode);
    
    currentOriginalNode = currentOriginalNode.next;
  }

  currentOriginalNode = head;

  for (let i = 0; i < newNodes.length; i++) {
    const currentNewNode = newNodes[i];
    if (currentOriginalNode.random) {
      const randomIndex = randomLookup(head, currentOriginalNode.random);
      currentNewNode.random = newNodes[randomIndex];
    }
    if (i < newNodes.length - 1) currentNewNode.next = newNodes[i + 1];
    currentOriginalNode = currentOriginalNode.next;
  }

  return newNodes[0];
};

const randomLookup = (list, nodePointer) => {
  let index = 0;
  let checkedNode = list;
  while (checkedNode) {
    if (nodePointer === checkedNode) return index;
    checkedNode = checkedNode.next;
    index++;
  }
  return list;
};

/*
Naive Solution
set array newNodes init = []
set currentOriginalNode = head
traverse the original list till the end
    each level create new node object | set val from current original node | set next & random to null | push to newNodes
    set currentOriginalNode to currentOriginalNode.next
create function randomLookup traverse on original list apply node pointer
    set index | init 0
    set checkedNode = head
    start while loop with condition checkedNode.next not null
        if node pointer equal to checkedNode return index
        else checkedNode = checkedNode.next && increase index
set currentOriginalNode to head again
iterate on newNodes
    set const currentNewNode to newNodes[i]
    if currentOriginalNode.random not null call randomLookup function & store index to const randomIndex | set currentNewNode.random to newNodes[randdomIndex]
    if i not equal last index set currentNewNode.next to newNodes[i + 1]
return newNodes[0]
*/
