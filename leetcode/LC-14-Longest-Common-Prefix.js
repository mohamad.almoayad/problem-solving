/**
 * @param {string[]} strs
 * @return {string}
 */
const longestCommonPrefix = (strs) => {
    let firstString = strs[0]
    if (strs.length === 1) return firstString;
    let firstStringLength = firstString.length;
    let commonPrefix = '';
    let isFounded = true;

    for (let x = 0; x < firstStringLength; x++) {
        let leftSubString = firstString.substring(0, firstStringLength - x);
        let rightSubString = firstString.substring(x);
        console.log(leftSubString, rightSubString)
        for (let i = 1; i < strs.length; i++) {
            if (strs[i].substring(0, firstStringLength - x) === leftSubString) {
                commonPrefix = leftSubString;
                rightSubString = leftSubString;
            } else if (strs[i].substring(x) === rightSubString) {
                commonPrefix = rightSubString;
                leftSubString = rightSubString;
            } else {
                isFounded = false;
                commonPrefix = '';
                break;
            }
        }
        if (isFounded) return commonPrefix;
        else isFounded = true;
    }

    return '';

};

const case1 = ["flower", "flow", "flight"];
const case2 = ["dog", "racecar", "car"];
const case3 = ["abcxyz", "lkabc", "nmxyz"];
const case4 = ["vb", "b"];
const case5 = ["aca", "cba"];
const case6 = ["haty", "pacb"];

const result = longestCommonPrefix(case5);
console.log(result);