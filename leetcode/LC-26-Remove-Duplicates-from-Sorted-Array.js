/**
 * @param {number[]} nums
 * @return {number}
 */
const removeDuplicates = (nums) => {
  if (nums.length === 1) return 1;
  let currentTarget = nums[0];
  let repeatedElementsCounter = 0;
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] === currentTarget) {
      repeatedElementsCounter++;
    } else {
      nums[i - repeatedElementsCounter] = nums[i];
      currentTarget = nums[i];
    }
  }
  const k = nums.length - repeatedElementsCounter;
  console.log(nums.slice(0, k));
  return k;
};

const case1 = [1, 1, 2];
const case2 = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
const case3 = [3, 3];
const result = removeDuplicates(case2);
console.log(result);

/*
set variable holds current target element value | init nums[0]
set variable holds repeated elements counter | init 0
start iteration over array from index 1
    check if nums[i] === current target value
    if equal increase repeated elements counter
    else if not equal assign the current index value to the index ( i - counter ) then set the current target value to the index value
after iteration set k to array length - counter
return k
*/
