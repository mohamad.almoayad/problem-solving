/**
 * @param {string} pattern
 * @return {string}
 */
var smallestNumber = function (pattern) {
  const length = pattern.length;
  let iCounter = 0;
  let result = [];

  for (let i = 0; i < length; ) {
    if (pattern.charAt(i) == "I") {
      result[i] = ++iCounter;
      i++;
    } else {
      i += recursive(i);
    }
  }

  function recursive(index) {
    if (pattern.charAt(index) == "I" || index == length) {
      iCounter++;
      result[index] = iCounter;
      return 1;
    }
    const m = recursive(index + 1);
    iCounter++;
    result[index] = iCounter;
    return m + 1;
  }
  if (pattern.charAt(length - 1) == "I") result.push(iCounter + 1);
  return result.join("");
};

console.log(smallestNumber("IIIDIDDD"));
console.log(smallestNumber("DIDIDIDI"));
//"IIIDDIDD"
// 123654987
