/**
 * @param {string} s
 * @return {string}
 */
var reverseVowels = function (s) {
  const vowels = { a: "", e: "", i: "", o: "", u: "" };
  let leftPointer = 0;
  let rightPointer = s.length - 1;
  let leftChar;
  let rightChar;
  let leftPart = "";
  let rightPart = "";
  while (leftPointer < rightPointer) {
    leftChar = s.charAt(leftPointer).toLowerCase();
    rightChar = s.charAt(rightPointer).toLowerCase();
    if (leftChar in vowels && rightChar in vowels) {
      leftPart += s.charAt(rightPointer);
      rightPart = s.charAt(leftPointer) + rightPart;
      leftPointer++;
      rightPointer--;
    } else {
      if (!(leftChar in vowels)) {
        leftPart += s.charAt(leftPointer);
        leftPointer++;
      }
      if (!(rightChar in vowels)) {
        rightPart = s.charAt(rightPointer) + rightPart;
        rightPointer--;
      }
    }
  }
  if (leftPointer == rightPointer) leftPart += s.charAt(leftPointer);
  return leftPart + rightPart;
};

console.log(reverseVowels("leetcode"));

//leetcode
// l -
// le - e
// le - de
// leo - ede
// leot - cede
