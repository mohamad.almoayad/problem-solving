/**
 * @param {number[][]} people
 * @return {number[][]}
 */
const reconstructQueue = (people = []) => {
  let lastIndex = people.length - 1;
  let startIndex = 0;
//   while (startIndex < lastIndex) {
//     let firstIndex = lastIndex - 1;
//     let secondIndex = lastIndex;
//     while (firstIndex >= 0) {
//       if (people[firstIndex][1] < people[secondIndex][1])
//         switchArrayElements(people, firstIndex, secondIndex);
//       else if (people[firstIndex][1] === people[secondIndex][1]) {
//         if (people[firstIndex][0] < people[secondIndex][0])
//           switchArrayElements(people, firstIndex, secondIndex);
//       }
//       firstIndex--;
//       secondIndex--;
//     }
//     startIndex++;
//   }

  people.sort((a, b) => {
      if(a[1] === b[1]) {
          return b[0] - a[0];
      }
      return b[1] - a[1];
  });
//   console.log('people', people)
  const result = [people.pop()];
  while (people.length > 0) {
    const currentMan = people.pop();
    const h = currentMan[0];
    let k = currentMan[1];
    if (k === 0) result.push(currentMan);
    else {
      for (let i = 0; i < result.length; i++) {
        if (k > 0) {
          if (result[i][0] >= h) k--;
        } else {
          if (result[i][0] >= h) {
            result.splice(i, 0, currentMan);
            break;
          }
        }
        if (i === result.length - 1) {
          result.push(currentMan);
          break;
        }
      }
    }
  }
  return result;
};
const switchArrayElements = (people, firstIndex, secondIndex) => {
  const temp = people[secondIndex];
  people[secondIndex] = people[firstIndex];
  people[firstIndex] = temp;
};
const case1 = [
  [7, 0],
  [4, 4],
  [7, 1],
  [5, 0],
  [6, 1],
  [5, 2],
];
const case2 = [
  [6, 0],
  [5, 0],
  [4, 0],
  [3, 2],
  [2, 2],
  [1, 4],
];
const result = reconstructQueue(case1);
console.log(result);

/*
  sort people array descending first on k then on h
  set result array | init people[0] 
  start while loop on people array | condition length > 0
      each lap pop last array elemnt | set to currentMan
      set h = set number of k = currentMan[0]
      set number of k = currentMan[1]
      loop on result array
          compare h with result[i]
          if result[i][0] >= h | decrease k--
          if k equal 0 append currentMan in i + 1 index
  return result
  */
