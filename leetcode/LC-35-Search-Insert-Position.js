/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
const searchInsert = (nums, target) => {
  if (target <= nums[0]) return 0;
  if (target === nums[nums.length - 1]) return nums.length - 1;
  if (target > nums[nums.length - 1]) return nums.length;

  let firstIndex = 0;
  let lastIndex = nums.length - 1;
  if (lastIndex - firstIndex === 1) return lastIndex;
  while (true) {
    const middleIndex = Math.floor((lastIndex + firstIndex) / 2);
    const middleValue = nums[middleIndex];
    if (target === middleValue) return middleIndex;
    else if (target > middleValue) firstIndex = middleIndex;
    else if (target < middleValue) lastIndex = middleIndex;
    if (lastIndex - firstIndex === 1) return lastIndex;
  }
};

const result = searchInsert();

/* 
check if target less than or equal nums[0] || larger than or equal nums[nums.length - 1]
if less || equal index 0 return 0
if equal last index return last index
if larger than last index return nums.length
set firstIndex = 0
set lastIndex = nums.length - 1
start while loop with true
if lastIndex - firstIndex > 1
    get the middle index between firstIndex & lastIndex & set to middleIndex
    set the value of middle index to middleValue
    compare the target with middleValue
    if the target equal middleValue return the middleIndex
    else if the target > middleValue set the fiestIndex to middleIndex
    else if the target < middleValue set the lastIndex to the middleIndex
else if lastIndex - firstIndex === 1
    return lastIndex
*/
