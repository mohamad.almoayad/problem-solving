/**
 * @param {string} s
 * @return {number}
 */
const countSubstrings = (s) => {
  if (s.length === 1) return 1;
  let firstPointer = 0;
  let lastPointer = 1;
  let result = 0;
  while (firstPointer < s.length) {
    if (lastPointer - firstPointer === 1) result++;
    else {
      const subString = s.slice(firstPointer, lastPointer);
      if (checkPalindromicStatus(subString)) result++;
    }
    lastPointer++;
    if (lastPointer > s.length) {
      firstPointer++;
      lastPointer = firstPointer + 1;
    }
  }
  return result;
};

const checkPalindromicStatus = (string) => {
  let leftChar = 0;
  let rightChar = string.length - 1;
  while (leftChar < rightChar) {
    if (string[leftChar++] !== string[rightChar--]) return false;
  }
  return true;
};

const case1 = "abc";
const case2 = "aaa";
const case3 = "aabcMcbaa";
const result = countSubstrings(case3);
console.log(result);

/*
set first pointer | init 0
set last pointer | init 0
set result | init 0
start while loop on condition first pointer < string length
    each lap:
    if last pointer - first pointer = 1 increase result by 1
    else: 
        sub string from first pointer char to last pointer char
        check palindromic with function ()
            set substring left char | init 0
            set substring right char | init sub string length - 1
            start while loop on condition left char < right char
                if left char ! equal right char return false
                increase left char && decrease right char
                if left char > or equal right char return true
    increase last pointer
    if last pointer > string length increase start pointer && set last pointer to start pointer + 1
return result
*/
