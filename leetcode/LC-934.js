/**
 * @param {number[][]} grid
 * @return {number}
 */

function shortestBridge(grid) {
  let length = grid.length;
  let founded = [];
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  let stepsCounter = Infinity;

  function boundriesChecker(x, y) {
    return x >= 0 && x < length && y >= 0 && y < length;
  }

  (function findIslanPiece(grid) {
    for (let x = 0; x < length; x++) {
      for (let y = 0; y < length; y++) {
        const piece = grid[x][y];
        if (piece === 1) {
          grid[x][y] = 2;
          founded.push([x, y]);
          markFirstIslandPieces(x, y);
          return;
        }
      }
    }
  })(grid);

  function markFirstIslandPieces(x, y) {
    for (let [row, column] of directions) {
      const xTarget = x + row;
      const yTarget = y + column;
      const isValidPosition = boundriesChecker(xTarget, yTarget);
      if (isValidPosition && grid[xTarget][yTarget] == 1) {
        grid[xTarget][yTarget] = 2;
        founded.push([xTarget, yTarget]);
        markFirstIslandPieces(xTarget, yTarget);
      }
    }
  }

  function countPathToSecondIsland(x, y, counter = 0, visited = {}) {
    // console.log(visited, !([x, y].toString() in visited));
    if (!([x, y].toString() in visited)) {
      visited[[x, y].toString()] = true;
      for (let [row, column] of directions) {
        const xTarget = x + row;
        const yTarget = y + column;
        const isValidPosition = boundriesChecker(xTarget, yTarget);

        if (isValidPosition) {
          //   console.log("Reach ", xTarget, yTarget, counter);
          if (grid[xTarget][yTarget] === 1) {
            if (counter < stepsCounter) stepsCounter = counter;
          } else if (counter < stepsCounter) countPathToSecondIsland(xTarget, yTarget, counter + 1, { ...visited });
        }
      }
    }
  }

  for (let [x, y] of founded) {
    countPathToSecondIsland(x, y, 0, {});
  }
  //   console.log(grid);
  return stepsCounter;
}
// init grid length
// init founded array
// init directions array
// init stepsCounter - set value to infinty
// init boundries functions to check position within boundries

/* Step 1 */
// find one island node
// -    mark the node & push to founded array
/* Step 2 */
// DFS within the directions to find all island nodes
// -    mark each founded node & push to founded array
/* Step 3 */
// Iterate on founded array
// -    DFS within directions starting with counter = 0
//      -   if not within boundries return
//      -   if value > 1 return
//      -   if value equal 1 return
//          -   if counter < stepsCounter update stepsCounter
//          -   return
//      -   increment counter
//          -   if counter > stepsCounter return
//          -   else update stepsCounter
//      -   recursive with updated counter

// console.log(
//   shortestBridge([
//     [0, 1, 1, 0],
//     [0, 0, 0, 0],
//     [0, 0, 0, 0],
//     [0, 1, 1, 0],
//   ])
// );

console.log(
  shortestBridge([
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0],
  ])
);

function minFlips(grid) {
  const n = grid.length;
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  let found = false;

  // Helper function to perform DFS and mark the first island
  function dfs(i, j) {
    if (i < 0 || i >= n || j < 0 || j >= n || grid[i][j] !== 1) {
      return;
    }
    grid[i][j] = 2; // Mark the cell as visited
    for (const [dx, dy] of directions) {
      dfs(i + dx, j + dy);
    }
  }

  // Helper function to perform DFS and find the second island
  function findSecondIsland(i, j) {
    if (i < 0 || i >= n || j < 0 || j >= n) {
      return false;
    }
    if (grid[i][j] === 0) {
      return true;
    }
    if (grid[i][j] === 1) {
      grid[i][j] = 2; // Mark the cell as visited
      for (const [dx, dy] of directions) {
        if (findSecondIsland(i + dx, j + dy)) {
          return true;
        }
      }
    }
    return false;
  }

  // Helper function to perform DFS and count the number of cells in an island
  function dfsCount(i, j) {
    if (i < 0 || i >= n || j < 0 || j >= n || grid[i][j] !== 1) {
      return 0;
    }
    grid[i][j] = 2; // Mark the cell as visited
    let count = 1;
    for (const [dx, dy] of directions) {
      count += dfsCount(i + dx, j + dy);
    }
    return count;
  }

  // Step 1: Find the first island and mark it
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 1) {
        dfs(i, j);
        found = true;
        break;
      }
    }
    if (found) {
      break;
    }
  }

  // Step 2: Find the second island and calculate the minimum number of flips
  let minFlips = Infinity;
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 1) {
        if (findSecondIsland(i, j)) {
          minFlips = Math.min(minFlips, dfsCount(i, j) - 1);
        }
      }
    }
  }

  return minFlips;
}
