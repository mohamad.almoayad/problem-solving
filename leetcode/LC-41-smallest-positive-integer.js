var firstMissingPositive = function (nums = []) {
  let result = 1;
  nums.sort((a, b) => a - b);
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > 0)
      if (nums[i] <= result) result = nums[i] + 1;
      else break;
  }
  console.log(result);
  return result;
};

firstMissingPositive([1, 1]);


