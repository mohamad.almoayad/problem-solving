/**
 * @param {string} s
 * @return {string}
 */
const longestPalindrome = (s) => {
  if (s.length === 1) return s;
  let foundedChars = {};
  let longestPalindromicSubstring = "";

  foundedChars[s.charAt(0)] = [0];
  for (let i = 1; i < s.length; i++) {
    let underCheckChar = s.charAt(i);
    if (underCheckChar in foundedChars) {
      let domain = foundedChars[underCheckChar];
      for (let x = 0; x < domain.length; x++) {
        let newSubString = s.slice(domain[x], i + 1);
        let checkSubString = checkPalindromicSubstring(newSubString);
        if (checkSubString) {
          if (newSubString.length > longestPalindromicSubstring.length)
            longestPalindromicSubstring = newSubString;
          break;
        }
      }
      domain.push(i);
    } else foundedChars[underCheckChar] = [i];
  }
  if (longestPalindromicSubstring.length === 0) return s.charAt(0);
  return longestPalindromicSubstring;
};

const checkPalindromicSubstring = (subString) => {
  let startPointer = 0;
  let endPointer = subString.length - 1;

  while (true) {
    if (subString.charAt(startPointer) === subString.charAt(endPointer)) {
      startPointer++;
      endPointer--;
    } else {
      return false;
    }
    if (startPointer === endPointer || startPointer > endPointer) break;
  }
  return true;
};

const case1 = "cbbd";
const case2 = "abcyacayc";
const case3 = "oxhbo";
const case4 = "xbasmsabg";
const case5 = "nn";
const case6 = "aacabdkacaa";

const result = longestPalindrome(case6);
console.log(result);

/*
set founded chars object key => char && value => index
set longest Palindromic string = ''
start iterating on string
check if the char exist in the founded chars object
if not exist add
if exist call function check Palindromic Substring between them 
| if true check the currrent longest Palindromic string length if larget set to the new sub string

check Palindromic Substring function | parameters (substring)
set subStringIndexesTerminals = []
set start && end pointers
set pointersState = "-"
start while loop with true condition
check equality between characters at start pointer position & end pointer position
if false empty subStringIndexesTerminals then decrease the  end pointer then set pointersState = "+"
check equality again
if false empty subStringIndexesTerminals then increse the  start pointer then set pointersState = "-"
check again
if true && if subStringIndexesTerminals is empty add the pointers values to the array then increase start pointer && decrease end pointer
if end pointer === start pointer || start pointer > end pointer break 
if subStringIndexesTerminals not empty return substring from the the first index position and the second index position
else return the first char
*/

//     let subStringIndexesTerminals = [];
//     let startPointer = 0;
//     let endPointer = s.length - 1;
//     let pointersAction = false;
//     while (true) {
//         if( s.charAt(startPointer) === s.charAt(endPointer) ){
//             if (subStringIndexesTerminals.length ===  0 ) subStringIndexesTerminals = [startPointer, endPointer];
//             startPointer++;
//             endPointer--;
//         } else {
//             subStringIndexesTerminals = [];
//             if (!pointersAction) endPointer--;
//             else if (pointersAction) startPointer++;
//             pointersAction = !pointersAction;
//         }
//         if(startPointer === endPointer || startPointer > endPointer) break;
//     }

//     if ( subStringIndexesTerminals.length ===  0 ) return s.charAt(0);
//     return s.slice(subStringIndexesTerminals[0], subStringIndexesTerminals[1] + 1);
