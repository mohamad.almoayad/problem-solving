const pushDominoes = (dominoes) => {
        if(dominoes.length < 2) return dominoes;
        const lastIndex = dominoes.length - 1;
        let result = '';
        const setStandingPiecesStatus = (index, status) => {
            if(index === 0 && dominoes.charAt(1) === 'L') {
                return "L";
            }
            if(index === lastIndex && dominoes.charAt(lastIndex - 1) === 'R') {
                return "R";
            }
            let toStartPointer = index -1;
            let toEndPointer = index + 1;
            let previousR = 0;
            let nextL = 0;
            while (toStartPointer >= 0 || toEndPointer <= lastIndex){
                if(toStartPointer >= 0){
                   if(dominoes.charAt(toStartPointer) === '.' ) toStartPointer--;
                    else if(dominoes.charAt(toStartPointer) === 'L') toStartPointer = -1;
                    else if(dominoes.charAt(toStartPointer) === 'R'){
                        previousR = index - toStartPointer;
                        toStartPointer = -1;
                    } 
                }
                
                if(toEndPointer <= lastIndex){
                   if(dominoes.charAt(toEndPointer) === '.' ) toEndPointer++;
                    else if(dominoes.charAt(toEndPointer) === 'R') toEndPointer = lastIndex + 1;
                    else if(dominoes.charAt(toEndPointer) === 'L'){
                        nextL = toEndPointer - index;
                        toEndPointer = lastIndex + 1;
                    } 
                }
                
            }
            if(previousR > nextL && nextL > 0) return 'L';
            else if(previousR > nextL && nextL === 0) return 'R';
            else if (nextL > previousR && previousR > 0) return 'R';
            else if (nextL > previousR && previousR === 0) return 'L';
            return ".";
        } 
        
        for(let i = 0; i < dominoes.length; i++){
            if(dominoes.charAt(i) === '.') {
                result = result + setStandingPiecesStatus(i, 0);
            }else result = result + dominoes.charAt(i);
        }
        return result;
    };

    const case1 = ".L.R...LR..L..";
    console.log(pushDominoes(case1))