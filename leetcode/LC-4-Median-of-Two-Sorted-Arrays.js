/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
const findMedianSortedArrays = (nums1, nums2) => {
  let medianArrayLength =
    (nums1.length + nums2.length) % 2 === 0
      ? (nums1.length + nums2.length) / 2 + 1
      : Math.ceil((nums1.length + nums2.length) / 2);
  let medianIndexes =
    (nums1.length + nums2.length) % 2 === 0
      ? [medianArrayLength - 1, medianArrayLength]
      : [medianArrayLength];
  let numsOnePointer = 0;
  let numsTwoPointer = 0;
  let medianTotalValues = 0;
  for (let i = 1; i <= medianArrayLength; i++) {
    let currentValue = 0;
    if (numsOnePointer < nums1.length && numsTwoPointer < nums2.length) {
      if (nums1[numsOnePointer] < nums2[numsTwoPointer]) {
        currentValue = nums1[numsOnePointer];
        numsOnePointer++;
      } else {
        currentValue = nums2[numsTwoPointer];
        numsTwoPointer++;
      }
    } else if (numsOnePointer < nums1.length) {
      currentValue = nums1[numsOnePointer];
      numsOnePointer++;
    } else if (numsTwoPointer < nums2.length) {
      currentValue = nums2[numsTwoPointer];
      numsTwoPointer++;
    }
    if (medianIndexes.includes(i)) medianTotalValues += currentValue;
  }
  if ((nums1.length + nums2.length) % 2 === 0) {
    return medianTotalValues / 2;
  }
  return medianTotalValues;
};

const case1 = [[1, 3], [2]];
const case2 = [
  [1, 2],
  [3, 4],
];
const case3 = [
  [1, 3],
  [2, 7],
];
const case4 = [
  [2, 2, 4, 4],
  [2, 2, 4, 4],
];
const case5 = [[], [1, 2, 3, 4, 5]];
const case6 = [[], [1, 2, 3, 4]];
const case7 = [[1], [2, 3, 4]];
const case8 = [
  [1, 2, 3, 7, 11, 13],
  [-1, 4, 5, 9, 12, 14],
];

const result = findMedianSortedArrays(...case8);
console.log(result);
