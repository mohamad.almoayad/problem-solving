const findSubstring = (string = "", words = []) => {
  if (words.length === 0) return [];
  const wordLength = words[0].length;
  let results = [];
  const wordsString = words.join("");
  let lettersBank = {};
  let startIndex = 0;
  let startIndexsContainer = [];

  for (let i in wordsString) {
    let currentChar = wordsString.charAt(i);
    if (currentChar in lettersBank) {
      lettersBank[currentChar]++;
    } else {
      lettersBank[currentChar] = 1;
    }
  }

  findSubstringRecursivly(startIndex);

  function findSubstringRecursivly(startIndex) {
    // Base Case
    if (startIndex + wordsString.length > string.length) return;

    let lettersBankCopy = { ...lettersBank };
    let wordsCopy = [...words];
    let word = "";

    if (!results.includes(startIndex))
      for (let i = startIndex; i < startIndex + wordsString.length; i++) {
        let currentChar = string.charAt(i);
        word = word + currentChar;
        if (
          currentChar in lettersBankCopy &&
          lettersBankCopy[currentChar] > 0
        ) {
          if (word.length === wordLength) {
            let checkWordExistIndex = wordsCopy.findIndex(
              (wordElement) => wordElement === word
            );
            if (checkWordExistIndex > -1) {
              wordsCopy.splice(checkWordExistIndex, 1);
            } else {
              startIndex++;
              if (!startIndexsContainer.includes(startIndex)) {
                startIndexsContainer.push(startIndex);
                findSubstringRecursivly(startIndex);
              }
              break;
            }
            word = "";
          }
          lettersBankCopy[currentChar]--;
        } else if (
          currentChar in lettersBankCopy &&
          lettersBankCopy[currentChar] === 0
        ) {
          startIndex++;
          if (!startIndexsContainer.includes(startIndex)) {
            startIndexsContainer.push(startIndex);
            return findSubstringRecursivly(startIndex);
          }
          break;
        } else {
          startIndex = i + 1;
          if (!startIndexsContainer.includes(startIndex)) {
            startIndexsContainer.push(startIndex);
            return findSubstringRecursivly(startIndex);
          }
          break;
        }
        if (i === startIndex + wordsString.length - 1) {
          if (wordsCopy.length === 0) results.push(startIndex);
          startIndex++;
          if (!startIndexsContainer.includes(startIndex)) {
            startIndexsContainer.push(startIndex);
            return findSubstringRecursivly(startIndex);
          }
        }
      }
  }
  return results;
};

// findSubstring("barfoofoobarthefoobarman", ["bar", "foo", "the"]);
findSubstring("foobarfoothebarfoobarman", ["bar", "foo", "the"]);
// findSubstring("aaaaaaaaaaaaaa", ["aa", "aa"]);
