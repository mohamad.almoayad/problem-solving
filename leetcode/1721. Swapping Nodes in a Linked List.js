/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var swapNodes = function(head, k) {
    const arr = [];
    const linkedList = {val: -1, next: null};

    let currentNode = head;
    while(currentNode) {
        arr.push(currentNode.val);
        currentNode = currentNode.next;
    }

    const rightSwapped = arr[k - 1];
    const leftSwapped = arr[arr.length - k];
    // console.log(arr, rightSwapped, leftSwapped)
    currentNode = linkedList;
    for( const [index, value] of arr.entries()) {
        let val = value;
        if(index == k -1) val = leftSwapped;
        else if(index == arr.length - k) val = rightSwapped;
        const newNode = {
            val: val, next: {}
        }
        currentNode.next = newNode;
        currentNode = currentNode.next;
        
    }

    console.log(linkedList.next, linkedList.next.next)
    return linkedList.next;
};