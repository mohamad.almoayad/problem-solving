/**
 * @param {string} s
 * @return {string}
 */
var finalString = function (s) {
  let forword = "";
  let reversed = "";
  let shouldReverse = false;
  let i = 0;
  let char;
  while (i < s.length) {
    char = s.charAt(i);
    if (s.charAt(i) == "i") shouldReverse = !shouldReverse;
    else {
      forword = shouldReverse ? char + forword : forword + char;
      reversed = shouldReverse ? reversed + char : char + reversed;
    }
    i++;
  }
  if (shouldReverse) return reversed;
  return forword;
};

console.log(finalString("abcidioei"));
// abcidioei  abc - cba
// cbadi      dabc - cbad
// dabc       dabcoe - eocbad
