const multiply = (num1, num2) => {
  let resultContainer = [];

  for (let x = num1.length - 1; x >= 0; x--) {
    for (let y = num2.length - 1; y >= 0; y--) {
      let carryIndex = x + y;
      let currentIndex = x + y + 1;
      let multiplicationResult =
        parseInt(num1.charAt(x)) * parseInt(num2.charAt(y));
      if (isNaN(resultContainer[carryIndex])) resultContainer[carryIndex] = 0;
      if (isNaN(resultContainer[currentIndex]))
        resultContainer[currentIndex] = 0;
      let sum = resultContainer[currentIndex] + multiplicationResult;
      resultContainer[currentIndex] = sum % 10;
      resultContainer[carryIndex] += Math.floor(sum / 10);
    }
  }
  resultContainer = resultContainer.join("");
  if (resultContainer.charAt(0) === "0")
    resultContainer = resultContainer.substr(1);
  return resultContainer;
};
const getDigit = (char) => {
  switch (char) {
    case "0":
      return 0;
    case "1":
      return 1;
    case "2":
      return 2;
    case "3":
      return 3;
    case "4":
      return 4;
    case "5":
      return 5;
    case "6":
      return 6;
    case "7":
      return 7;
    case "8":
      return 8;
    case "9":
      return 9;
  }
};
const result = multiply("123456789", "987654321");
// const result = multiply("456", "123");
console.log(result, typeof result);
