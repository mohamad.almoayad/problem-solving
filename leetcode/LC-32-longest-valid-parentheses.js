const longestValidParenthesesX = (parentheses) => {
  if (parentheses.length === 0) return 0;
  let result = 0;
  let openedStack = [-1];
  for (let i = 0; i < parentheses.length; i++) {
    if (parentheses[i] === "(") {
      openedStack.push(i);
    } else if (openedStack.length === 1) {
      openedStack[0] = i;
    } else {
      openedStack.pop();
      result = Math.max(result, i - openedStack[openedStack.length - 1]);
    }
  }
  return result;
};

const longestValidParentheses = (parentheses) => {
  if (parentheses.length === 0) return 0;
  let result = 0;
  let openparenthesesCount = 0;
  let closedparenthesesCount = 0;
  let linkedClosedParenthesis = 0;
  for (let i = 0; i < parentheses.length; i++) {
    if (parentheses[i] === "(") {
      openparenthesesCount++;
    } else {
      closedparenthesesCount++;
    }
    if (closedparenthesesCount > openparenthesesCount) {
      openparenthesesCount = 0;
      closedparenthesesCount = 0;
      linkedClosedParenthesis = 0;
    } else {
      if (parentheses[i] === ")") {
        linkedClosedParenthesis++;
        if (closedparenthesesCount * 2 > result) {
          if (closedparenthesesCount > linkedClosedParenthesis) {
            let stringLength = closedparenthesesCount * 2;
            let startIndex = i - stringLength + 1;
            let openedInRange = 0;
            let closedInRange = 0;
            let rangeResult = true;
            while (stringLength >= 2 && stringLength > result) {
              for (let index = startIndex; index <= i; index++) {
                if (parentheses[index] === "(") {
                  openedInRange++;
                } else {
                  closedInRange++;
                }
                if (closedInRange > openedInRange) {
                  rangeResult = false;
                  break;
                }
              }
              if (
                rangeResult &&
                openedInRange === closedInRange &&
                openedInRange + closedInRange > result
              ) {
                result = openedInRange + closedInRange;
                stringLength = 0;
              } else {
                stringLength -= 2;
                startIndex += 2;
                openedInRange = 0;
                closedInRange = 0;
                rangeResult = true;
              }
            }
          } else if (linkedClosedParenthesis * 2 > result)
            result = linkedClosedParenthesis * 2;
        }
      } else if (parentheses[i] === "(") {
        linkedClosedParenthesis = 0;
      }
    }
  }
  return result;
};

const case1 = "(()";
const case2 = ")()())";
const case3 = "()(()";
const case4 = "(()(((()";
const case5 = "(()()";
const case6 = "(()(()))" + "(((((())())))"; // "(()(()))(((((())())))"
const case7 = "(((((())())))" + "(()(()))";
const case8 = "))))((()((";
longestValidParentheses(case7);
