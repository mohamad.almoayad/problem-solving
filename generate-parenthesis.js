let result = [];

function combineParenthesisRecursively(n, opened, closed, string) {
  if (string.length === n * 2) {
    result.push(string);
    return;
  }
  if (opened < n) {
    combineParenthesisRecursively(n, opened + 1, closed, string + "(");
  }
  if (closed < opened) {
    combineParenthesisRecursively(n, opened, closed + 1, string + ")");
  }
}

combineParenthesisRecursively(3, 0, 0, "");
// console.log(result);

let a = { y: 20 };
a.x = a;

console.log(a.x, a.y);
console.log("arr", JSON.stringify(a));
