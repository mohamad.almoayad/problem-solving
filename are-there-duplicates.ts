const areThereDuplicates = <T>(args: T[]): boolean => {
    // create object to hold args elements in keys
    interface LooseObject {
        [key: string]: string
    }
    let elementsObj: LooseObject = {};
    // create two pointers
    let leftPointer = 0;
    let rightPointer = args.length - 1
    // loop on args using two pointers
    while(leftPointer < rightPointer) {
        const leftArg = String(args[leftPointer]);
        const rightArg = String(args[rightPointer]);
        // each lap check the element exist, if exist return true
        if(leftArg in elementsObj || rightArg in elementsObj ) return true;
        elementsObj[leftArg] = '';
        elementsObj[rightArg] = '';
        // move pointers one place
        leftPointer++;
        rightPointer--;
    }
    //return false
    return false;
}

const case1 = areThereDuplicates([1, 2, 2]);
const case2 = areThereDuplicates(['a', 'b', 'c', 'a'])
console.log(case1, case2);