function fibonacci(target) {
  // add whatever parameters you deem necessary - good luck!
  if(target < 1 )return 0;
  else if (target <= 2) return 1;
  return fibonacci(target - 1) + fibonacci(target - 2);
}

console.log(fibonacci(28));
