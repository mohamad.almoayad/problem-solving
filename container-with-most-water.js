const calculateMaxArea = (height = []) => {
  let leftPointer = 0;
  let rightPointer = height.length - 1;
  let leftHand = height[leftPointer];
  let rightHand = height[rightPointer];
  let maxHeight = leftHand < rightHand ? leftHand : rightHand;
  let maxWidth = height.length - 1;
  let maxArea = maxHeight * maxWidth;

  while (leftPointer !== rightPointer) {
    leftHand < rightHand ? leftPointer++ : rightPointer--;
    leftHand = height[leftPointer];
    rightHand = height[rightPointer];

    let potentialMaxHeight =
      leftHand < rightHand ? height[leftPointer] : height[rightPointer];
    if (potentialMaxHeight > maxHeight) {
      let potentialMaxArea = potentialMaxHeight * (rightPointer - leftPointer);
      if (potentialMaxArea > maxArea) maxArea = potentialMaxArea;
    }
  }
  return maxArea;
};

const result = calculateMaxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]);
console.log(result);

// Accepted Bad Solution
/* let maxArea = 0;
  for (let i = 0; i < height.length; i++) {
    for (let x = i + 1; x < height.length; x++) {
      let width = x - i;
      let minHeight = height[i] < height[x] ? height[i] : height[x];
      let area = minHeight * width;
      if (area > maxArea) maxArea = area;
    }
  } */
