class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(value) {
    const newNode = { ...new Node(value) };
    if (!this.root) {
      this.root = newNode;
      return;
    }
    let currentNode = this.root;
    while (true) {
      if (value >= currentNode.value) {
        if (!currentNode.right) {
          currentNode.right = newNode;
          return;
        }
        currentNode = currentNode.right;
      } else {
        if (!currentNode.left) {
          currentNode.left = newNode;
          return;
        }
        currentNode = currentNode.left;
      }
    }
    return;
  }

  lookup(value) {
    let currentNode = this.root;
    let goDeeper = true;
    while (goDeeper) {
      if (currentNode.value === value) return currentNode;
      let nextNode = null;
      if (value > currentNode.value) {
        nextNode = currentNode.right;
      } else {
        nextNode = currentNode.left;
      }
      if (!nextNode) {
        return null;
      } else {
        if (nextNode.value === value) return nextNode;
        currentNode = nextNode;
      }
    }
    return currentNode;
  }

  remove(value) {
    if (!this.root) return;
    if (this.root.value === value) {
      if (this.root.right && !this.root.left) {
        this.root = this.root.right;
      } else if (!this.root.right && this.root.left) {
        this.root = this.root.left;
      } else {
        this.root = null;
      }
      // console.log('FINISHED', this.root)
      return;
    }
    let parentNode = null;
    let currentNode = this.root;
    while (true) {
      if (currentNode.value === value) {
        // console.log('NODE FOUND', currentNode)
        if (currentNode.right && !currentNode.left) {
          if (currentNode.value > parentNode.value)
            parentNode.right = currentNode.right;
          if (currentNode.value < parentNode.value)
            parentNode.left = currentNode.right;
        } else if (!currentNode.right && currentNode.left) {
          if (currentNode.value > parentNode.value)
            parentNode.right = currentNode.left;
          if (currentNode.value < parentNode.value)
            parentNode.left = currentNode.left;
        } else if (!currentNode.right && !currentNode.left) {
          if (currentNode.value > parentNode.value) parentNode.right = null;
          if (currentNode.value < parentNode.value) parentNode.left = null;
        } else {
          if (currentNode.right.right && !currentNode.right.left) {
            if (currentNode.value > parentNode.value)
              parentNode.right = currentNode.right;
            if (currentNode.value < parentNode.value)
              parentNode.left = currentNode.right;
          } else if (!currentNode.right.right && currentNode.right.left) {
            if (currentNode.value > parentNode.value)
              parentNode.right = currentNode.left;
            if (currentNode.value < parentNode.value)
              parentNode.left = currentNode.left;
          } else if (!currentNode.right.right && !currentNode.right.left) {
            if (currentNode.value > parentNode.value)
              parentNode.right = currentNode.right;
            if (currentNode.value < parentNode.value)
              parentNode.left = currentNode.right;
          } else {
            let lastLeftParentNode = null;
            let lastLeftNode = currentNode.right;
            console.log(
              "IS LAST NODE LEFT NULL",
              lastLeftNode.left === null,
              lastLeftNode.left
            );
            while (lastLeftNode.left) {
              lastLeftParentNode = lastLeftNode;
              lastLeftNode = lastLeftNode.left;
            }
            console.log("LAST LEFT PARENT", lastLeftParentNode);
            console.log("LAST LEFT", lastLeftNode);
            lastLeftParentNode.left = lastLeftNode.right
              ? lastLeftNode.right
              : null;
            lastLeftNode.right = currentNode.right;
            lastLeftNode.left = currentNode.left;
            if (currentNode.value > parentNode.value)
              parentNode.right = lastLeftNode;
            if (currentNode.value < parentNode.value)
              parentNode.left = lastLeftNode;
            return this;
          }

          //console.log('LAST LEFT', lastLeftNode)
        }
        return this;
      }

      if (value > currentNode.value) {
        parentNode = currentNode;
        currentNode = currentNode.right;
      } else if (value < currentNode.value) {
        parentNode = currentNode;
        currentNode = currentNode.left;
      }
    }
  }
}

const tree = new BinarySearchTree();
tree.insert(9);
tree.insert(4);
tree.insert(6);
tree.insert(20);
tree.insert(170);
tree.insert(15);
tree.insert(17);
tree.insert(13);
tree.insert(180);
tree.insert(150);
tree.insert(160);
tree.insert(140);
// tree.insert(145)
tree.insert(1);
tree.remove(20);
// JSON.stringify(traverse(tree.root));
//tree.lookup(77)
//      9
//    4              20
//  1  6      15              170
//           13 17       150        180
//                   140     160
//                      145

function traverse(node) {
  const tree = { value: node.value };
  tree.left = node.left === null ? null : traverse(node.left);
  tree.right = node.right === null ? null : traverse(node.right);
  console.log(tree);
  return tree;
}
