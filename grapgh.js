class Graph {
  constructor() {
    this.adjacencyList = {};
  }

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) this.adjacencyList[vertex] = {};
  }

  addEdge(v1, v2) {
    const adgecencyList = this.adjacencyList;
    if (adgecencyList[v1] && adgecencyList[v2]) {
      adgecencyList[v1][v2] = true;
      adgecencyList[v2][v1] = true;
    }
    return adgecencyList;
  }

  dfsRecursive(vertex) {
    const adgecencyList = this.adjacencyList;
    if (!adgecencyList[vertex]) return null;
    let visited = {};
    let verticies = [];
    // return Object.keys(adgecencyList[vertex])
    (function dfs(vertex) {
      visited[vertex] = true;
      verticies.push(vertex);
      for (let v of Object.keys(adgecencyList[vertex])) {
        if (!visited[v]) dfs(v);
      }
      // Alternative iteration
      //   Object.keys(adgecencyList[vertex]).forEach((v) => {
      //     if (!visited[v]) return dfs(v);
      //   });
    })(vertex);

    return verticies;
  }
}

export default Graph;
