var result = 0;
function runFactorial(number){
    if(result === 0) result = number;
    if(number === 1) return result;
    result *= number - 1;
    // console.log(result);
    number--;
    return runFactorial(number);
    // runFactorial(runFactorial(runFactorial(runFactorial()))) 
}
// runFactorial(5);

function findFactorial(number){
    if(number === 2) return 2;
    return number * findFactorial(number - 1); // number = 4 // 4 * findFactorial(3) * findFactorial(2)
}

console.log(findFactorial(5))