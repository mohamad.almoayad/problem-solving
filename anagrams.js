

function validateAnagrams ( str1, str2) {
        // check length equality
        if(str1.length != str2.length) return false;
        // fill each string in object
        let obj1 = {};
        let obj2 = {};
        for(let char of str1) {
            // console.log(char)
            obj1[char] = (obj1[char] || 0) + 1;
        }
        for(let char of str2) {
            // console.log(char)
            obj2[char] = (obj2[char] || 0) + 1;
        }
        // compare the two objects keys & values similarity
        for(let key of Object.keys(obj1)) {
            console.log("key: ", key)
            if(!key in obj2 || obj1[key] !== obj2[key]) return false
        }
        return true;
    }


    const case1 = validateAnagrams("samira", "asmira");
    const case2 = validateAnagrams("awful", "fawlu");
    const case3 = validateAnagrams("streaam", "streams");
    console.log(case1, case2, case3);
    