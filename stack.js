class NewNode {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class NewStack {
  constructor() {
    this.top = null;
    this.bottom = null;
    this.length = 0;
  }

  push(value) {
    let newNode = new NewNode(value);
    if (this.length === 0) {
      this.top = newNode;
      this.bottom = this.top;
    } else {
      let currentTop = this.top;
      newNode.next = currentTop;
      this.top = newNode;
    }
    this.length++;
    return this;
  }

  pop() {
      if(!this.top) return null;
      if(this.top === this.bottom) this.bottom = null;
      this.top = this.top.next;
      this.length--;
      return this;
  }
  peek() {
      return this.top;
  }
}
let newStack = new NewStack();
newStack.push("Google");
newStack.push("Apple");
newStack.push("Youtube");
newStack.pop();
newStack.pop();
newStack.pop();
console.log(newStack);  
// console.log(newStack.peek());
