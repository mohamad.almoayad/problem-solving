function power(base, exponent) {
  if (exponent === 0) return 1;
  else if (exponent === 1) return base;
  return base * power(base, exponent - 1);
}

console.log(power(3, 4));
