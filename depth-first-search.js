import createBinaryTree from "./create-binary-tree.js";

function depthFirstSearchPreOrder(tree) {
  let treeNodes = [];
  let currentNode = tree.root || null;
  traverseTree(currentNode);
  function traverseTree(node) {
    if (!node.value) return;
    treeNodes.push(node.value);
    if (node.left) traverseTree(node.left);
    if (node.right) traverseTree(node.right);
  }
  return treeNodes;
}
function depthFirstSearchPostOrder(tree) {
  let treeNodes = [];
  let currentNode = tree.root || null;
  traverseTree(currentNode);
  function traverseTree(node) {
    if (!node.value) return;
    if (node.left) traverseTree(node.left);
    if (node.right) traverseTree(node.right);
    treeNodes.push(node.value);
  }
  return treeNodes;
}
function depthFirstSearchInOrder(tree) {
  let treeNodes = [];
  let currentNode = tree.root || null;
  traverseTree(currentNode);
  function traverseTree(node) {
    if (!node.value) return;
    if (node.left) traverseTree(node.left);
    treeNodes.push(node.value);
    if (node.right) traverseTree(node.right);
  }
  return treeNodes;
}

// const tree = {
//   root: {
//     value: 50,
//     left: {
//       value: 40,
//       left: {
//         value: 30,
//         left: {
//           value: 20,
//         },
//         right: {
//           value: 32,
//           left: {
//             value: 31,
//           },
//         },
//       },
//       right: {
//         value: 45,
//         left: {
//           value: 42,
//         },
//       },
//     },
//     right: {
//       value: 60,
//       left: {
//         value: 55,
//       },
//       right: {
//         value: 70,
//       },
//     },
//   },
// };

//                     50
//             40              60
//        30        45       55      70
//     20    32   42
//         31
// [50, 40, 30, 20, 32,31, 45, 42, 60, 55,70]

const tree = { root: createBinaryTree(16) };

console.log(depthFirstSearchPreOrder(tree));
console.log(depthFirstSearchPostOrder(tree));
console.log(depthFirstSearchInOrder(tree));
