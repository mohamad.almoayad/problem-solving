const divideTwoIntegers = (dividend, divisor) => {
  let counter = 0;
  let result = 0;

  let sign = dividend > 0 === divisor > 0;
  dividend = Math.abs(dividend);
  divisor = Math.abs(divisor);

  let baseDivisor = divisor;
  if (dividend < divisor) return 0;
  let dividendLength = dividend.toString().length;
  let divisorLength = divisor.toString().length;
  let zeros = "";

  if (dividendLength - divisorLength > 1)
    for (let i = divisorLength; i < dividendLength - 1; i++) {
      zeros = zeros + "0";
    }

  divisor = parseInt(divisor.toString() + zeros);
  while (dividend - divisor >= 0) {
    dividend = dividend - divisor;
    counter++;
    if (dividend - divisor < 0) {
      result += parseInt(counter.toString() + zeros);
      counter = 0;
      if (zeros.length >= 1) {
        zeros = zeros.substring(0, zeros.length - 2);
        divisor = parseInt(baseDivisor + zeros);
      }
    }
  }
  
  if (!sign) result = -result;
  if (result > Math.pow(2, 31) - 1) return Math.pow(2, 31) - 1;
  if (result < Math.pow(-2, 31)) return Math.pow(-2, 31);

  return result;

  /* let counter = 0;
  let sign = dividend > 0 === divisor > 0;
  dividend = Math.abs(dividend);
  divisor = Math.abs(divisor);
  if (dividend < divisor) return 0;

  let multiplications = 0;
  while (dividend - divisor >= 0 && multiplications >= -1) {
    let change = false;
    while (true) {
      if (dividend - ((divisor << 1) << multiplications) < 0) {
        break;
      }
      change = true;
      multiplications++;
    }
    if (change) {
      counter += 1 << multiplications;
      dividend -= divisor << multiplications;
    }
    multiplications--;
  }
  if (!sign) counter = -counter;
  if (counter > Math.pow(2, 31) - 1) return Math.pow(2, 31) - 1;
  if (counter < Math.pow(-2, 31)) return Math.pow(-2, 31);

  console.log(counter);
  return counter; */
};

divideTwoIntegers(-2147483648, -3);
