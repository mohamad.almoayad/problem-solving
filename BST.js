class BST {
  constructor(node) {
    if (!(node instanceof TreeNode)) return this;
    this.root = node ?? null;
  }
  insert(value) {
    if (!this.root) return;
    let currentNode = this.root;
    const node = new TreeNode(value);
    const newValue = node.value;
    while (true) {
      if (newValue === currentNode.value) return this;
      if (newValue < currentNode.value) {
        if (!currentNode.left) {
          currentNode.left = node;
          return this;
        }
        currentNode = currentNode.left;
      } else {
        if (!currentNode.right) {
          currentNode.right = node;
          return this;
        }
        currentNode = currentNode.right;
      }
    }
  }

  search(value) {
    if (!this.root) return;
    let currentNode = this.root;
    while (true) {
      if (currentNode.value === value) return true;
      else if (value < currentNode.value) {
        if (currentNode.left) currentNode = currentNode.left;
        else return false;
      } else {
        if (currentNode.right) currentNode = currentNode.right;
        else return false;
      }
    }
  }
}

class TreeNode {
  constructor(value) {
    this.value = value;
    this.right = null;
    this.left = null;
  }
}

let tree = new BST(new TreeNode(7));
tree.insert(10);
tree.insert(4);
tree.insert(6);
tree.insert(12);
tree.insert(8);
tree.insert(5);
tree.insert(6);
console.log("is no 2 exist? ", tree.search(2));

console.log(tree.root);
