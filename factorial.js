function factorial(number) {
  if (number < 0) return 0;
  if (number < 2) return 1;
  return number * factorial(--number);
}

console.log(factorial(7));
