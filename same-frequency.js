function sameFrequency(num1, num2) {
    console.log(arguments, Object.keys(arguments) )
  // parse numbers to strings
  const str1 = num1.toString();
  const str2 = num2.toString();
  // check equality of the two strings if not return false
  if (str1.length !== str2.length) return false;
  // loop on first string and extract into object
  let charsObj = {};
  for (let char of str1) {
    charsObj[char] = (charsObj[char] + 1) || 1;
  }
  // loop on second string and check each char in the object
  for (let char of str2) {
    // if not exist reeturn false
    if (!(char in charsObj) || charsObj[char] === 0) return false;
    // if exist decrement the value by 1
    charsObj[char]--;
  }

  return true;
}

const case1 = sameFrequency(3589578, 5879385);
const case2 = sameFrequency(182,281);
const case3 = sameFrequency(22, 222);
const case4 = sameFrequency(35895783, 58793855);

console.log(case1, case2, case3, case4);
