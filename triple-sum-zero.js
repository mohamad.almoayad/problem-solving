const getAllTripleSumZero = (nums = []) => {
  let triplesContainer = [];
  nums.sort((a, b) => a - b);

  for (let i = 0; i + 2 < nums.length; i++) {
    if (i > 0 && nums[i] === nums[i - 1]) continue;
    let inStartIndex = i + 1;
    let inEndIndex = nums.length - 1;

    while (inStartIndex < inEndIndex) {
      let combinationSum = nums[i] + nums[inStartIndex] + nums[inEndIndex];
      if (combinationSum === 0) {
        triplesContainer.push([nums[i], nums[inStartIndex], nums[inEndIndex]]);
        inEndIndex--;
        while (
          inStartIndex < inEndIndex &&
          nums[inEndIndex] === nums[inEndIndex + 1]
        )
          inEndIndex--;
      } else if (combinationSum > 0) inEndIndex--;
      else if (combinationSum < 0) inStartIndex++;
    }
  }

  return triplesContainer;

  /* for (let i = 0; i < nums.length; i++) {
    for (let x = 0; x < nums.length; x++) {
      if (x !== i) {
        container.push({
          triple: [nums[i], nums[x]],
          indexes: [i, x],
          weight:
            nums[i].toString().charCodeAt(0) + nums[x].toString().charCodeAt(0),
        });
      }
    }
  }

  for (let i = 0; i < container.length; i++) {
    for (let x = 0; x < nums.length; x++) {
      let checkExistIndex = container[i]["indexes"].includes(x);
      if (!checkExistIndex) {
        let temp = [...container[i]["triple"], nums[x]];
        let weight = container[i]["weight"] + nums[x].toString().charCodeAt(0);
        let totalValues = temp.reduce((total, number) => total + number);
        if (totalValues === 0) {
          let checkDuplicateIndex = triplesWeights.filter(
            (item) => item["weight"] === weight
          );

          if (checkDuplicateIndex.length === 0) {
            triplesWeights.push({
              index: triplesContainer.length,
              weight: weight,
            });
            triplesContainer.push([...temp]);
          } else {
            let checkEquality = false;
            for (let i = 0; i < checkDuplicateIndex.length; i++) {
              let index = checkDuplicateIndex[i]["index"];
              if (compareTwoArraysRecursivly(triplesContainer[index], temp)) {
                checkEquality = true;
                break;
              }
            }
            if (!checkEquality) {
              triplesWeights.push({
                index: triplesContainer.length,
                weight: weight,
              });
              triplesContainer.push([...temp]);
            }
          }
        }
      }
    }
  }

  console.log(triplesContainer.length);
  return triplesContainer;

  function compareTwoArraysRecursivly(arrOne = [], arrTwo = []) {
    let firstArray = [...arrOne];
    let secondArray = [...arrTwo];
    if (firstArray.length !== secondArray.length) return false;
    if (firstArray.length === 0) return secondArray.length === 0;
    let index = secondArray.findIndex((item) => item === firstArray[0]);
    if (index > -1) {
      firstArray.splice(0, 1);
      secondArray.splice(index, 1);
    } else {
      firstArray = [];
    }
    return compareTwoArraysRecursivly(firstArray, secondArray);
  }

  function filterListRecursive() {
    // Base Case
    if (triplesContainer.length === 0) return finalList;

    if (finalList.length === 0) finalList.push(triplesContainer[0]);

    for (let x in finalList) {
      let static = triplesContainer[0];
      let dynamic = finalList[x];
      if (compareTwoArraysRecursivly(static, dynamic)) {
        triplesContainer.splice(0, 1);
        break;
      }
      // * type of x is string *
      if (x == finalList.length - 1) {
        finalList.push([...static]);
        triplesContainer.splice(0, 1);
      }
    }
    return filterListRecursive();
  } */

  // return filterListRecursive();

  /* let matrix = [];
  let numsOneThirdSize = Math.floor(nums.length / 3);
  for (let i = 0; i < 3; i++) {
    if (i < 2) matrix.push(nums.splice(0, numsOneThirdSize));
    else matrix.push(nums);
  }

  let rowIndex = 0;
  let container = [];

  function combineArraysNumbersRecursivly(array) {
    if (rowIndex === matrix.length - 1) return container;
    container = [];
    for (let x = 0; x < array.length; x++) {
      let localContainer = [];
      for (y = 0; y < matrix[rowIndex + 1].length; y++) {
        if (array[x].length > 0) {
          localContainer.push(...array[x]);
        } else {
          localContainer.push(array[x]);
        }
        localContainer.push(matrix[rowIndex + 1][y]);
        container.push(localContainer);
        localContainer = [];
      }
    }
    rowIndex++;
    return combineArraysNumbersRecursivly(container);
  }

  function compareTwoArraysRecursivly(arrOne = [], arrTwo = []) {
    let firstArray = [...arrOne];
    let secondArray = [...arrTwo];
    if (firstArray.length !== secondArray.length) return false;
    if (firstArray.length === 0) return secondArray.length === 0;
    let index = secondArray.findIndex((item) => item === firstArray[0]);
    if (index > -1) {
      firstArray.splice(0, 1);
      secondArray.splice(index, 1);
    } else {
      firstArray = [];
    }
    return compareTwoArraysRecursivly(firstArray, secondArray);
  }

  const result = combineArraysNumbersRecursivly(matrix[0]);
  let final = [];
  let charachtersValues = [];
  result.forEach((list) => {
    let totalValues = list.reduce((total, number) => total + number);
    if (totalValues === 0) {
      let checkExist = false;
      for (let i in final) {
        if (compareTwoArraysRecursivly(final[i], list)) {
          checkExist = true;
          break;
        }
      }
      if (!checkExist) {
        final.push(list);
      }
    }
  });
  return final; */
};

// const result = getAllTripleSumZero([
//   5, -9, -11, 9, 9, -4, -4, 6, -15, -8, 7, 5, -3, 5, 0, -1, 6, -15, -8, 7,
// ]);
// const result = getAllTripleSumZero([
//   5, -9, -11, 9, 9, -4, 14, 10, -11, 1, -13, 11, 10, 14, -3, -3, -4, 6, -15, 6,
//   6, -13, 7, -11, -15, 10, -8, 13, -14, -12, 12, 6, -6, 8, 0, 10, -11, -8, -2,
//   -6, 8, 0, 12, 3, -9, -6, 8, 3, -15, 0, -6, -1, 3, 9, -5, -5, 4, 2, -15, -3, 5,
//   13, -11, 7, 6, -4, 2, 11, -5, 7, 12, -11, -15, 1, -1, -9, 10, -8, 1, 2, 8, 11,
//   -14, -4, -3, -12, -2, 8, 5, -1, -9, -4, -3, -13, -12, -12, -10, -3, 6, 1, 12,
//   3, -3, 12, 11, 11, 10,
// ]);
// const result = getAllTripleSumZero([
//   -7, -10, -1, 3, 0, -7, -9, -1, 10, 8, -6, 4, 14, -8, 9, -15, 0, -4, -5, 9, 11,
//   3, -5, -8, 2, -6, -14, 7, -14, 10, 5, -6, 7, 11, 4, -7, 11, 11, 7, 7, -4, -14,
//   -12, -13, -14, 4, -13, 1, -15, -2, -12, 11, -14, -2, 10, 3, -1, 11, -5, 1, -2,
//   7, 2, -10, -5, -8, -10, 14, 10, 13, -2, -9, 6, -7, -7, 7, 12, -5, -14, 4, 0,
//   -11, -8, 2, -6, -13, 12, 0, 5, -15, 8, -12, -1, -4, -15, 2, -5, -9, -7, 12,
//   11, 6, 10, -6, 14, -12, 9, 3, -10, 10, -8, -2, 6, -9, 7, 7, -7, 4, -8, 5, -4,
//   8, 0, 3, 11, 0, -10, -9,
// ]);
const result = getAllTripleSumZero([
  12, 5, -12, 4, -11, 11, 2, 7, 2, -5, -14, -3, -3, 3, 2, -10, 9, -15, 2, 14,
  -3, -15, -3, -14, -1, -7, 11, -4, -11, 12, -15, -14, 2, 10, -2, -1, 6, 7, 13,
  -15, -13, 6, -10, -9, -14, 7, -12, 3, -1, 5, 2, 11, 6, 14, 12, -10, 14, 0, -7,
  11, -10, -7, 4, -1, -12, -13, 13, 1, 9, 3, 1, 3, -5, 6, 9, -4, -2, 5, 14, 12,
  -5, -6, 1, 8, -15, -10, 5, -15, -2, 5, 3, 3, 13, -8, -13, 8, -5, 8, -6, 11,
  -12, 3, 0, -2, -6, -14, 2, 0, 6, 1, -11, 9, 2, -3, -6, 3, 3, -15, -5, -14, 5,
  13, -4, -4, -10, -10, 11,
]);
// const result = getAllTripleSumZero([
//   10, -2, -12, 3, -15, -12, 2, -11, 3, -12, 9, 12, 0, -5, -4, -2, -7, -15, 7, 4,
//   -5, -14, -15, -15, -4, 10, 9, -6, 7, 1, 12, -6, 14, -15, 12, 14, 10, 0, 10,
//   -10, 3, 4, -12, 10, 7, -9, -7, -15, -8, -15, -4, 2, 9, -4, 3, -10, 13, -3, -1,
//   5, 5, -4, -15, 4, -11, 4, -4, 6, -11, -9, 12, 7, 11, 7, 2, -5, 13, 10, -5,
//   -10, 3, 7, 0, -3, 10, -12, 2, 9, -8, 8, -9, 13, 12, 13, -6, 8, 3, 5, -9, 7,
//   12, 10, -8, 0, 2, 1, 10, -7, -3, -10, -10, 7, 4, 5, -11, -8, 0, -2, -14, 8,
//   13, -8, -2, 10, 13,
// ]);
// const result = getAllTripleSumZero([-1, 0, 1, 2, -1, -4]);

console.log(result);