/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function (s, wordDict) {
  /**
   * @param {string} str
   */
  let result = false;
  let segments = {};
  for (let word of wordDict) {
    segments[word] = "";
  }
  const helper = (str, words = [], i) => {
    const subStr = str.substring(0, i);
    // console.log( subStr, i);

    if (subStr in segments) {
      if (i < str.length) helper(str.substring(i), words, 1);
      else {
        // console.log("End of string", subStr, i);
        result = true;
      }
    }
    if (i < str.length) {
      helper(str, words, i + 1);
    }
  };
  helper(s, wordDict, 1);
  //   console.log("=====", result);
  return result;
};

const wordBreak2 = (s, wordDict) => {
  let words = new Set(wordDict);
  let queue = [];
  let seen = new Array(s.length + 1).fill(false);
  queue.push(0);

  while (queue.length > 0) {
    let start = queue.shift();

    if (start == s.length) {
      console.log("block no#1: ", start);
      return true;
    }

    for (let end = start + 1; end <= s.length; end++) {
      console.log("new lap", start, end);
      if (seen[end]) {
        console.log("block no#2", end, seen[end]);
        continue;
      }

      if (words.has(s.substring(start, end))) {
        queue.push(end);
        seen[end] = true;
        console.log("block no#3:", start, end, queue, seen);
      }
    }
  }

  return false;
};

// leetscodeplatform (leet, leets, code, platform)

// console.log(wordBreak("leetcode", ["leet", "code"]));
// console.log(wordBreak2("leetscode", ["leet", "leets", "code"]));
// console.log(wordBreak("applepenapple", ["apple", "pen"]));
// console.log(wordBreak("catsandog", ["cats", "dog", "sand", "and", "cat"]));
// console.log(
//   wordBreak2("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", ["a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa", "aaaaaaaaaa"])
// );

let arr = ["apple", "banana", "orange", "kiwi"];
const [one, two, ...subArr] = arr;
let [...c] = arr
console.log(c)