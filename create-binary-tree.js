function createBinaryTree(minNodes = 16) {
    // Create a root node with a random value.
    const root = {
      value: Math.floor(Math.random() * 60) + 1,
      left: null,
      right: null,
    };
  
    // Recursively insert nodes into the tree.
    function insertNode(node, value) {
      if (value < node.value) {
        if (node.left === null) {
          node.left = {
            value: value,
            left: null,
            right: null,
          };
        } else {
          insertNode(node.left, value);
        }
      } else {
        if (node.right === null) {
          node.right = {
            value: value,
            left: null,
            right: null,
          };
        } else {
          insertNode(node.right, value);
        }
      }
    }
  
    // Insert 16 nodes into the tree.
    for (let i = 0; i < minNodes; i++) {
      const value = Math.floor(Math.random() * 60) + 1;
      insertNode(root, value);
    }
  
    // If the tree has less than 16 nodes, recursively insert more nodes until the tree has at least 16 nodes.
    if (countNodes(root) < minNodes) {
      createBinaryTreeWithAtLeast16Nodes(root, minNodes);
    }
  
    return root;
  }
  
  // Counts the number of nodes in a binary tree.
  function countNodes(node) {
    if (node === null) {
      return 0;
    } else {
      return 1 + countNodes(node.left) + countNodes(node.right);
    }
  }

export default createBinaryTree;