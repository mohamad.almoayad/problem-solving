var MultiDimensionArray = function (digits) {
  if (digits.length === 0) return [];
  const lettersMap = {
    2: ["a", "b", "c"],
    3: ["d", "e", "f"],
    4: ["g", "h", "i"],
    5: ["j", "k", "l"],
    6: ["m", "n", "o"],
    7: ["p", "q", "r", "s"],
    8: ["t", "u", "v"],
    9: ["w", "x", "y", "z"],
  };
  let container = [];
  if (digits.length === 1) {
    for (let i = 0; i < lettersMap[digits].length; i++) {
      container.push(lettersMap[digits][i]);
    }
    return container;
  }

  let matrix = [];
  let rowIndex = 0;

  for (let i = 0; i < digits.length; i++) {
    matrix.push(lettersMap[digits.charAt(i)]);
  }

  function getAllRowsCurrentIndexElementRecursive(array) {
    if (rowIndex === matrix.length - 1) return container;
    container = [];
    for (let i = 0; i < array.length; i++) {
      for (y = 0; y < matrix[rowIndex + 1].length; y++)
        container.push(array[i] + matrix[rowIndex + 1][y]);
    }
    rowIndex++;
    return getAllRowsCurrentIndexElementRecursive(container);
  }
  return getAllRowsCurrentIndexElementRecursive(matrix[rowIndex]);
};

const result = MultiDimensionArray("2");
console.log(result);
