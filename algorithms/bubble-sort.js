const numbers = [
  99, 44, 6, 2, 1, 5, 546, 63, 87, 283, 4, 0, -3, 18, 27, 33, -8, 67, 5, 46, 96,
  39, 12, -56, 184, -45, 34, -452, 95, 13, 302, -221, 63, 87, 26, -11,
];
let counter = 0;

function bubbleSort(array) {
  let isSorted = false;
  let tempNum = 0;
  for (let i = 0; i < array.length - 1; i++) {
    if (i === 0) isSorted = true;
    if (array[i] > array[i + 1]) {
      tempNum = array[i];
      array[i] = array[i + 1];
      array[i + 1] = tempNum;
      isSorted = false;
    }
    if (!isSorted && i === array.length - 2) i = -1;
  }
}

bubbleSort(numbers);
console.log(numbers);
