let currentIndex = 2;
let result = 1;
let firstNumber = 0;
let secondNumber = 1;

function getFibonacci(index) {
  if (currentIndex === 2) {
    currentIndex = 2;
    result = 1;
    firstNumber = 0;
    secondNumber = 1;
  }
  if (currentIndex === index) {
    return result;
  }
  let tempResult = result;
  result = result + secondNumber;
  firstNumber = secondNumber;
  secondNumber = tempResult;
  currentIndex++;
  return getFibonacci(index);
}

function recursivreFibonacci(n) {
  if (n === 2) return 1;
  if (n === 1) return 1;
  return recursivreFibonacci(n - 1) + recursivreFibonacci(n - 2);
}

/**
 * @param {Number} n
 */
function recursivreFibonacciDynamic(n, resultsCache = {}) {
  if (n in resultsCache) return resultsCache[n];
  if (n <= 2) {
    resultsCache[n] = 1;
    return 1;
  }
  resultsCache[n] = recursivreFibonacciDynamic(n - 1, resultsCache) + recursivreFibonacciDynamic(n - 2, resultsCache);
  return resultsCache[n];
}

// let x = getFibonacci(7); // 0 1 1 2 3 5 8 13 21 34 55 89
// let y = getFibonacci(6); // 0 1 1 2 3 5 8 13 21 34 55 89

// console.log(recursivreFibonacci(100));
// console.log(recursivreFibonacciDynamic(100));

function countGlasses(rows) {
  if (rows == 1) return 1;
  return countGlasses(rows - 1) + rows;
}

console.log(countGlasses(33));
