const numbers = [99, 44, 6, 2, 1, 5, 546, 63, 87, 283, 4, 0, -3, 18, 27, 33, -8, 67, 5, 46, 96, 39, 12, -56, 184, -45, 34, -452, 95, 13, 302, -221, 63, 87, 26, -11];

function selectionSort(array) {

  for (let i = 0; i < array.length; i++) {
    let smallest = array[i];
    let index = i;

    for (let x = i + 1; x < array.length; x++) {
      if (array[x] < array[index]) {
        // smallest = array[x];
        index = x;
      }
    }

    // ANOTHER WAY TO SWITCH VALUES WITHOUT TEMP VARUABLE
    [array[index], array[i]] = [array[i], array[index]]
    // array[index] = array[i];
    // array[i] = smallest; 
  }
}

selectionSort(numbers);
console.log(numbers)