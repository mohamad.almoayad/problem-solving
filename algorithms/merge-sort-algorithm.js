const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];
// console.log([4,7,3].slice(0));
function mergeSort(array) {
  // console.log("MergeSort", array);
  if (array.length === 1) {
    console.log("MergeSort", array);
    return array;
  }
  // Split Array in into right and left
  const length = array.length;
  const middle = Math.floor(length / 2);
  const left = array.slice(0, middle);
  const right = array.slice(middle);
  // console.log("MergeSort left:", left);
  // console.log("MergeSort right:", right);

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  console.log("Merge: ", left, right);
  const result = [];
  let leftIndex = 0;
  let rightIndex = 0;

  while (leftIndex < left.length && rightIndex < right.length) {
    // if (left[leftIndex] < right[rightIndex]) {
    //   result.push(left[leftIndex]);
    //   leftIndex++;
    // } else {
    //   result.push(right[rightIndex]);
    //   rightIndex++;
    // }
    left[leftIndex] < right[rightIndex] ? (result.push(left[leftIndex]) , leftIndex++) : (result.push(right[rightIndex]), rightIndex++);
    // console.log("WHILE LOOP: ", left, right, result);
  }

  // console.log(
  //   "RETURN MERGE: "
  //   ,result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex))
  // );
  const x = result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
  console.log("return merge function: ", x);
  return x;
}

const answer = mergeSort(numbers);
console.log("FInal Result: ", answer);
