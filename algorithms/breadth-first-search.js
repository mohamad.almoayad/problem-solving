class TreeNode {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}
class BFS {
  constructor() {
    this.root = null;
  }

  addTreeNode(value) {
    let newNode = new TreeNode(value);
    if (!this.root) this.root = newNode;
    else {
      let currentNode = this.root;
      while (true) {
        if (value < currentNode.value) {
          if (!currentNode.left) {
            currentNode.left = newNode;
            return this;
          }
          currentNode = currentNode.left;
        } else {
          if (!currentNode.right) {
            currentNode.right = newNode;
            return this;
          }
          currentNode = currentNode.right;
        }
      }
    }
  }
  breadthFirstSearch() {
    let nodesList = [];
    let queue = [this.root];
    while (queue.length) {
      let currentNode = queue.shift();
      nodesList.push(currentNode.value);
      if (currentNode.left) {
        queue.push(currentNode.left);
      }
      if (currentNode.right) {
        queue.push(currentNode.right);
      }
    }
    return nodesList;
  }

  printTree() {
    console.log("Print Tree: ");
  }
}
function traversePostOrder(node, list) {
  if (node.left) {
    traversePostOrder(node.left, list);
  }
  if (node.right) {
    traversePostOrder(node.right, list);
  }
  list.push(node.value);
  return list;
}

let bfs = new BFS();
bfs.addTreeNode(9);
bfs.addTreeNode(5);
bfs.addTreeNode(6);
bfs.addTreeNode(4);
bfs.addTreeNode(7);
bfs.addTreeNode(3);
bfs.addTreeNode(1);
bfs.addTreeNode(8);
bfs.addTreeNode(17);
bfs.addTreeNode(19);
bfs.addTreeNode(12);
bfs.addTreeNode(21);
bfs.addTreeNode(13);
bfs.addTreeNode(14);
bfs.addTreeNode(16);
bfs.addTreeNode(15);
console.log(traversePostOrder(bfs.root, []));
console.log(bfs.breadthFirstSearch());
bfs.printTree();
