let case1 = [8, 7, 2, 1, 0, 9, 6, -4, 11, 3, 27, 5];
const quickSort = (array, startIndex, endIndex) => {
  if (startIndex < endIndex) {
    const pivot = array[endIndex];
    let greaterPointer = startIndex - 1;
    for (let i = startIndex; i < endIndex; i++) {
      if (array[i] <= pivot) {
        greaterPointer++;
        let temp = array[i];
        array[i] = array[greaterPointer];
        array[greaterPointer] = temp;
      }
    }
    greaterPointer++;
    let temp = array[greaterPointer];
    array[greaterPointer] = pivot;
    array[endIndex] = temp;
    quickSort(array, startIndex, greaterPointer - 1);
    quickSort(array, greaterPointer + 1, endIndex);
  }
  case1 = [...array];
};

quickSort(case1, 0, case1.length - 1);
console.log(case1);
