class BinaryHeapTreeMax {
  constructor() {
    this.tree = [];
  }

  insert(node) {
    const tree = this.tree;
    tree.push(node);
    if (tree.length > 1) {
      let nodeCurrentIndex = tree.length - 1;
      let nodeParentIndex = Math.floor((nodeCurrentIndex - 1) / 2);
      while (tree[nodeCurrentIndex] > tree[nodeParentIndex]) {
        const tempValue = tree[nodeParentIndex];
        tree[nodeParentIndex] = tree[nodeCurrentIndex];
        tree[nodeCurrentIndex] = tempValue;
        nodeCurrentIndex = nodeParentIndex;
        nodeParentIndex = Math.floor((nodeCurrentIndex - 1) / 2);
        if (nodeParentIndex < 0) break;
      }
    }
    return this.tree;
  }

  extract() {
    const tree = this.tree;
    if(tree.length < 1) return null;
    if(tree.length < 2) return tree.pop();
    const extractedValue = tree[0];
    const end = tree.pop();
    let parentIndx = 0;
    tree[parentIndx] = end;
    while (true) {
      let leftChildIndx = 2 * parentIndx + 1;
      let righttChildIndx = 2 * parentIndx + 2;
      let leftChild = tree[leftChildIndx] || null;
      let rightChild = tree[righttChildIndx] || null;
      if (!leftChild && !rightChild) break;
      let maxIndex = leftChild > rightChild ? leftChildIndx : righttChildIndx;
      if (tree[parentIndx] > tree[maxIndex]) break;
      let temp = tree[maxIndex];
      tree[maxIndex] = tree[parentIndx];
      tree[parentIndx] = temp;
      parentIndx = maxIndex;
    }
    return extractedValue;
  }
}

const BHT = new BinaryHeapTreeMax();
BHT.insert(41);
BHT.insert(39);
BHT.insert(33);
BHT.insert(18);
BHT.insert(27);
BHT.insert(12);
BHT.insert(55);
console.log(BHT.tree);
BHT.extract();
BHT.extract();
BHT.extract();
BHT.extract();
BHT.extract();
BHT.extract();
console.log(BHT.extract());
