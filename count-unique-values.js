const countUniqueValues = (arr) => {
  // check array length if smaller than 2 return length
  if (arr.length < 2) return arr.length;
  // loop on array using pointer that moves on each element starting from index 1
  let firstPointer = 0;
  let secondPointer = 1;
  // the first pointer holds the index of the last unique element
  while (secondPointer < arr.length) {
    // each round check if values for both pointers indexes are different increment the first pointer by one then replace its index value by the second pointer value
    if (arr[firstPointer] !== arr[secondPointer]) {
      firstPointer++;
      arr[firstPointer] = arr[secondPointer];
    }
    // increment the second pointer index + 1
    secondPointer++;
  }
  // return the first pointer index + 1
  return ++firstPointer;
};

const case1 = countUniqueValues([-4, -4, -1, 0, 0, 3, 7, 7, 7, 11, 27, 27]);
const case2 = countUniqueValues([1,2,3,4,4,4,7,7,12,12,13]);
const case3 = countUniqueValues([-2,-1,-1,0,1]);
const case4 = countUniqueValues([7]);
const case5 = countUniqueValues([1,3]);
console.log(case1, case2, case3, case4, case5);
