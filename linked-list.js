class LinkedList {
  constructor(initValue) {
    this.head = {
      value: initValue,
      next: null,
      prev: null,
    };
    this.tail = this.head;
    this.length = 1;
  }

  append(value) {
    const newNode = {
      value: value,
      next: null,
      prev: null,
    };
    newNode.prev = this.tail;
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
  }

  prepend(value) {
    const newNode = {
      value: value,
      next: this.head,
      prev: null,
    };
    this.head.prev = newNode;
    this.head = newNode;
    this.length++;
  }

  insert(index, value) {
    if (!this.validateIndex(index)) return;
    if (index === 0) {
      this.prepend(value);
      return;
    }
    let preIndexNode = this.pointToPreIndexNode(index);
    const newNode = {
      value: value,
      next: null,
      prev: null,
    };
    newNode.next = preIndexNode.next;
    newNode.prev = preIndexNode;
    preIndexNode.next.prev = newNode;
    preIndexNode.next = newNode;
    this.length++;
    return;
  }

  remove(index) {
    if (!this.validateIndex(index)) return;
    let targetNode = this.pointToTargetNode(index);

    if (targetNode.prev !== null) targetNode.prev.next = targetNode.next;
    if (targetNode.next !== null) targetNode.next.prev = targetNode.prev;
    if (targetNode.prev === null) this.head = targetNode.next;
    if (targetNode.next === null) this.tail = targetNode.prev;

    this.length--;
    return;
  }

  reverse() {
    const values = this.printList();
    let currentNode = this.head;
    for (let i = values.length - 1; i >= 0; i--) {
      currentNode.value = values[i];
      currentNode = currentNode.next;
    }
    return;
  }

  pointToTargetNode(index) {
    let targetNode = this.head;
    for (let i = 0; i < index; i++) targetNode = targetNode.next;
    return targetNode;
  }

  pointToPreIndexNode(index) {
    let preIndexNode = this.head;
    for (let i = 1; i < index; i++) preIndexNode = preIndexNode.next;
    return preIndexNode;
  }

  validateIndex(index) {
    if (index >= this.length) return false;
    return true;
  }

  printList() {
    const array = [];
    let currentNode = this.head;
    while (currentNode !== null) {
      console.log(currentNode.value);
      array.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return array;
  }
}

const newLinkedList = new LinkedList(7);
newLinkedList.append(11);
newLinkedList.append(15);
newLinkedList.prepend(5);
newLinkedList.insert(1, 9);
newLinkedList.remove(3);
newLinkedList.reverse();
newLinkedList.printList();
console.log(newLinkedList);
