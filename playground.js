let string = "ABCDEF";
// console.log(string.slice(0, 1) + string.slice(2, string.length));
// string = string.substring(0,3) +'M' +string.substring(4);
string = "M" + string.substring(1)
// console.log(string, typeof null)
function randomizeArray(A) {
  if (A.length === 1) return 0;
  let negatives = [];
  let mostNegative = { index: null, value: 0 };
  let sum = 0;
  let relocations = 0;
  for (let i = 0; i < A.length; i++) {
    sum += A[i];
    // console.log(sum);
    if (A[i] < 0) {
      negatives.push({
        index: i,
        value: A[i],
      });
      if (A[i] < mostNegative["value"])
        mostNegative = { index: i, value: A[i] };
    }
    if (sum < 0) {
      let mostNegativeIndex = mostNegative["index"];
      let mostNegativeValue = mostNegative["value"];
      // console.log("mostNegativeValue", mostNegativeValue);
      sum -= mostNegativeValue;
      mostNegative = { index: null, value: 0 };
      for (let x = 0; x < negatives.length; x++) {
        if (negatives[x]["index"] == mostNegativeIndex) {
          negatives.slice(x, 1);
          relocations++;
        } else {
          if (negatives[x] < mostNegative["value"])
            mostNegative = {
              index: negatives[x]["index"],
              value: negatives[x]["value"],
            };
        }
      }
    }
  }
  // console.log("relocations", relocations);
}

// const result = randomizeArray([3]);

// console.log(result);

/* function counter(){
  let i = 0;
  while( i <= 110000){
    postMessage(i);
    i++;
    if(i > 100000) console.log('i: ', i)
  }
}
counter(); */


const getMaxNonRepeated = (string = "") => {
  let startIndex = 1;
  let maxLength = 1;
  let counter = 1;
  let char = string[0];
  let stringMap = {};
  stringMap[char] = 0;
  while(true) {
    if(startIndex === string.length || string.length - startIndex <= maxLength) break;
    
    char = string[startIndex];
    if(char in stringMap) {
      startIndex = stringMap[char] + 1;
      stringMap = {}; 
      counter = 0;    
    } else {
       stringMap[char] = startIndex;
      counter++;
      if(counter > maxLength) maxLength = counter;
      startIndex++;
    }
  }
  // console.log("result" ,maxLength);
  return maxLength;
}

// getMaxNonRepeated("abcabcbb");

class Coupons {
  constructor(coupons) {
      this.coupons = coupons
  }
  
  addCoupon (coupon) {
      this.coupons.push(coupon);
  }
  
  getCoupons() {
      return this.coupons;
  }
  
  searchCoupons(string) {
      const coupons = this.getCoupons();
      let results = [];
      let couponsFounded = {};
      for(let i = 0; i < coupons.length; i++) {
          const tags = coupons[i]['tags'];
          console.log(tags);
          for(let x =  0; x < tags.length; x++) {
            console.log(tags[x]);
              if(tags[x].startsWith(string) ) {
                  const couponId = coupons[i]['id'];
                  if(couponId in couponsFounded === false) {
                      couponsFounded[couponId] = '';
                      results.push(couponId);
                  }
              }
          }
          
      }
      return results;
  }
  
  /* 
 tagsMap = {
      tagName: [coupon_1_Id, coupon_2_id];
  }
  
  iterate on tagsMap Object Keys
  each lap will compare with the string search word
  if we get the tagName valid will check values "coupons Ids" and add distinct to the result
  
  */
}

const coupons = new Coupons([]);
// coupons.addCoupon({id: 'ABCD123', tags: ['grocery', 'beverage']});
// coupons.addCoupon({id: 'ABCD234', tags: ['grocery', 'snacks']});

const result = coupons.searchCoupons('be');
// console.log("result", result);

let s = ["a", "b", "c"];
s.pop();
(function(){
  var a = b= 5;
  console.log("b= ", b)
})()
// console.log(s[s.length - 1])