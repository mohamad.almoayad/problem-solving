const exportUrlDomain = (url = "") => {
  let startIndex = -1;
  let lastIndex = -1;
  let target = ".";
  for (let i = 0; i < url.length; i++) {
    const char = url.charAt(i);
    // if()
    if (char === ".") target = "/";
    else if (target === "/" && char === "/") {
      lastIndex = i;
      break;
    }
  }
  return url.slice(0, lastIndex);
};

const result = exportUrlDomain("https://tcrn.ch/3yJH0vv.");
console.log(result);

