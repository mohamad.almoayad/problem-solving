import Graph from "./grapgh.js";

function graphDfsRecursive() {
  let graph = new Graph();
  graph.addVertex("Alexandria");
  graph.addVertex("Dubai");
  graph.addVertex("Geddah");
  graph.addVertex("Amman");
  graph.addVertex("AbuDhabi");
  graph.addVertex("Bairut");

  graph.addEdge("Alexandria", "Dubai");
  graph.addEdge("Alexandria", "Amman");
  graph.addEdge("Alexandria", "Geddah");
  graph.addEdge("Dubai", "Geddah");
  graph.addEdge("Dubai", "AbuDhabi");
  graph.addEdge("Amman", "AbuDhabi");
  graph.addEdge("Bairut", "AbuDhabi");
  console.log(graph.adjacencyList);

  const verticies = graph.dfsRecursive("Amman");
  console.log(verticies);
}

graphDfsRecursive();

// function inspectObject() {
//   let obj = {
//     name: "mohamad",
//     last: "almoayad",
//   };
//   console.log(obj);
//   for (let e in obj) {
//     console.log("for in: ", e);
//   }
//   for (let e of Object.keys(obj)) {
//     console.log("for of: ", e, obj[e]);
//   }
// }
// inspectObject();

