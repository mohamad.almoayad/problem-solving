/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
const search =  (nums, target) => {
    if (nums.length === 0) return -1;
    if (target < nums[0] || target > nums[nums.length - 1]) return -1;
    if (nums.length === 1) return 0;
    if (nums.length === 2) {
        return nums[0] === target ? 0 : 1;
    }

    let firstIndex = 0;
    let lastIndex = nums.length - 1;
    let midIndex = Math.floor(lastIndex / 2);
    let segmentLength = lastIndex - firstIndex;

    while (true) {
        if (segmentLength > 3) {
            if (nums[midIndex] === target) {
                return midIndex;
            } else if (target > nums[midIndex]) {
                firstIndex = midIndex + 1;
            } else if (target < nums[midIndex]) {
                lastIndex = midIndex - 1;
            }
            segmentLength = lastIndex - firstIndex;
            midIndex = Math.floor((firstIndex + lastIndex) / 2);

        } else {
            for (let i = firstIndex; i <= lastIndex; i++) {
                if (nums[i] === target) {
                    return i;
                }
            }
            return -1;
        }
    }
};