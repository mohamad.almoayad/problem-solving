function areThereDuplicates() {
  let obj = {};
  console.log(Object.values(arguments))
  for (let arg of arguments) {
    
    if (arg in obj) return true;
    obj[arg] = "";
  }
  return false;
}

console.log(areThereDuplicates(1, 2, 3));
console.log(areThereDuplicates(1, 2, 2));
console.log(areThereDuplicates("a", "b", "c", "a"));
