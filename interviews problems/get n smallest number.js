const getSmallestN = (arr = [], n = 0) => {
  let node = new linkedList();
  node.insert(arr[0]);
  for (let i = 1; i < arr.length; i++) {
    node.insert(arr[i]);
  }
  console.log(node.lookup(n));
};

class linkedList {
  constructor() {
    this.root = null;
  }

  insert(number) {
    if (!this.root) {
      this.root = {
        value: number,
        next: null,
      };
      return;
    }

    let currentNode = this.root;
    let newNode = {
      value: number,
      next: null,
    };
    if (newNode.value < currentNode.value) {
      newNode.next = currentNode;
      this.root = newNode;
      return this;
    }

    while (true) {
      if (!currentNode.next) {
        currentNode.next = newNode;
        return;
      } else if (newNode.value < currentNode.next.value) {
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        return;
      } else currentNode = currentNode.next;
    }
  }

  lookup(index) {
    let counter = 1;
    let currentNode = this.root;
    while (true) {
      if (counter === index) return currentNode.value;
      currentNode = currentNode.next;
      counter++;
    }
  }
}

getSmallestN([6, 3, 9, 15, 7, 2, 11], 2);

class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
  }

  insert(data) {
    const newNode = new Node(data);
    if (!this.head || data < this.head.data) {
      newNode.next = this.head;
      this.head = newNode;
    } else {
      let current = this.head;
      while (current.next !== null && data > current.next.data) {
        current = current.next;
      }
      newNode.next = current.next;
      current.next = newNode;
    }
  }

  lookup(n) {
    let counter = 0;
    let current = this.head;
    while (current) {
      counter++;
      if (counter === n) {
        return current.data;
      }
      current = current.next;
    }
    return null; // If n exceeds the number of elements in the list
  }
}

function findNthSmallestNumber(arr, n) {
  const linkedList = new LinkedList();

  for (let i = 0; i < arr.length; i++) {
    linkedList.insert(arr[i]);
  }

  return linkedList.lookup(n);
}

// Example Usage:
// const array = [7, 10, 4, 3, 20, 15];
// const nth = 3;
// const result = findNthSmallestNumber(array, nth);
// console.log(`The ${nth}th smallest number is: ${result}`);
