const getLongestPalindromic = (s) => {
  let longestPalindromic = {
    start: 0,
    end: 0,
    length: 0,
  };
  let lettersMap = {};

  for (let i = 0; i < s.length; i++) {
    if (s.charAt(i) in lettersMap) {
      let inBetweenChar = s.charAt(i - 1);
      let inBetwwen = true;
      for (let x = lettersMap[s.charAt(i)] + 1; x < i; x++) {
        if (s.charAt(x) !== inBetweenChar) {
          inBetwwen = false;
          break;
        }
      }
      if (inBetwwen) {
        if (longestPalindromic["length"] < i - lettersMap[s.charAt(i)] + 1) {
          longestPalindromic = {
            start: lettersMap[s.charAt(i)],
            end: i,
            length: i - lettersMap[s.charAt(i)] + 1,
          };
        }
      }
    }
    lettersMap[s.charAt(i)] = i;
  }
  console.log(longestPalindromic);
};

getLongestPalindromic("babad");
